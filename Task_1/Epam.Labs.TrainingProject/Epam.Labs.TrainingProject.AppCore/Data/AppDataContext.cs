﻿using Epam.Labs.TrainingProject.AppCore.Entities;
using Microsoft.EntityFrameworkCore;

namespace Epam.Labs.TrainingProject.AppCore.Data
{
    public class AppDataContext : DbContext
    {
        public AppDataContext(DbContextOptions<AppDataContext> options)
            : base(options)
        {
        }

        public DbSet<City> Cities { get; set; }
        public DbSet<Category> Categories { get; set; }
        public DbSet<Role> Roles { get; set; }
        public DbSet<AuthenticationData> AuthenticationDatas { get; set; }
        public DbSet<Culture> Cultures { get; set; }
        public DbSet<Status> Statuses { get; set; }
        public DbSet<Venue> Venues { get; set; }
        public DbSet<Event> Events { get; set; }
        public DbSet<User> Users { get; set; }
        public DbSet<Ticket> Tickets { get; set; }
        public DbSet<Order> Orders { get; set; }

        protected override void OnModelCreating(ModelBuilder modelBuilder)
        {
            modelBuilder.Entity<City>().ToTable("City");
            modelBuilder.Entity<Role>().ToTable("Role");
            modelBuilder.Entity<AuthenticationData>().ToTable("AuthenticationData");
            modelBuilder.Entity<Culture>().ToTable("Culture");
            modelBuilder.Entity<Status>().ToTable("Status");
            modelBuilder.Entity<Venue>().ToTable("Venue");
            modelBuilder.Entity<Event>().ToTable("Event");
            modelBuilder.Entity<User>().ToTable("User");
            modelBuilder.Entity<Ticket>().ToTable("Ticket");
            modelBuilder.Entity<Order>().ToTable("Order");
            modelBuilder.Entity<Category>().ToTable("Category");
        }
    }
}
