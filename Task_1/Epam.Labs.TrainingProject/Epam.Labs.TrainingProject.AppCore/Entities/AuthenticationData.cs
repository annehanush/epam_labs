﻿using System;
using System.ComponentModel.DataAnnotations;

namespace Epam.Labs.TrainingProject.AppCore.Entities
{
    /// <summary>
    /// Authentication (username, email, password, user, role).
    /// </summary>
    public class AuthenticationData
    {
        public Guid Id { get; set; }

        [Required]
        public string Username { get; set; }

        [Required]
        public string Email { get; set; }

        [Required]
        public string Password { get; set; }

        public Guid UserId { get; set; }
        public User User { get; set; }

        public Guid RoleId { get; set; }
        public Role Role { get; set; }
    }
}
