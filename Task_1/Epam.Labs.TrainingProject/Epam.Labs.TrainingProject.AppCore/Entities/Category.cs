﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;

namespace Epam.Labs.TrainingProject.AppCore.Entities
{
    public class Category
    {
        public Guid Id { get; set; }

        [Required]
        public string Name { get; set; }

        public ICollection<Event> Events { get; set; }
    }
}
