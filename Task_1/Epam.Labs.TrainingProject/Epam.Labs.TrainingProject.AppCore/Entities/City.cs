﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;

namespace Epam.Labs.TrainingProject.AppCore.Entities
{
    /// <summary>
    /// City (id, name);
    /// </summary>
    public class City
    {
        public Guid Id { get; set; }

        [Required]
        public string Name { get; set; }

        public ICollection<Venue> Venues { get; set; }
    }
}
