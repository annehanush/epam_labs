﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;

namespace Epam.Labs.TrainingProject.AppCore.Entities
{
    /// <summary>
    /// Culture (id, name).
    /// </summary>
    public class Culture
    {
        public Guid Id { get; set; }

        [Required]
        public string Name { get; set; }

        public ICollection<User> Users { get; set; }
    }
}
