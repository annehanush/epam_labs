﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;

namespace Epam.Labs.TrainingProject.AppCore.Entities
{
    /// <summary>
    /// Event (id, name, date, banner(path to), venue, description).
    /// </summary>
    public class Event
    {
        public Guid Id { get; set; }

        [Required]
        public string Name { get; set; }

        [Required]
        public DateTime Date { get; set; }

        [Required]
        public string Banner { get; set; }

        public string Description { get; set; }

        public Guid VenueId { get; set; }
        public Venue Venue { get; set; }

        public Guid CategoryId { get; set; }
        public Category Category { get; set; }

        public ICollection<Ticket> Tickets { get; set; }
    }
}
