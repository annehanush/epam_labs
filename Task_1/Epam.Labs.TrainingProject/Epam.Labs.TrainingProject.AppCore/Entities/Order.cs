﻿using System;
using System.ComponentModel.DataAnnotations;

namespace Epam.Labs.TrainingProject.AppCore.Entities
{
    /// <summary>
    /// Order (id, ticket, status, buyer, trackNO).
    /// </summary>
    public class Order
    {
        public Guid Id { get; set; }

        [Required]
        public string TrackNO { get; set; }

        [Required]
        public DateTime Date { get; set; }

        public string RejectionComment { get; set; }

        public Guid? TicketId { get; set; }
        public Ticket Ticket { get; set; }

        public Guid StatusId { get; set; }
        public Status Status { get; set; }

        public Guid BuyerId { get; set; }
        public User Buyer { get; set; }
    }
}
