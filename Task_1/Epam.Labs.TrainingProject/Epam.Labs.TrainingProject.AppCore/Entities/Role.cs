﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;

namespace Epam.Labs.TrainingProject.AppCore.Entities
{
    /// <summary>
    /// Role (id, name).
    /// </summary>
    public class Role
    {
        public Guid Id { get; set; }

        [Required]
        public string Name { get; set; }

        public ICollection<AuthenticationData> AuthenticationDatas { get; set; }
    }
}
