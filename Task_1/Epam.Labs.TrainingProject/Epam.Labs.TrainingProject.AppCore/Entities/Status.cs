﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;

namespace Epam.Labs.TrainingProject.AppCore.Entities
{
    /// <summary>
    /// Status (id, name).
    /// </summary>
    public class Status
    {
        public Guid Id { get; set; }

        [Required]
        public string Name { get; set; }

        public ICollection<Ticket> Tickets { get; set; }
        public ICollection<Order> Orders { get; set; }
    }
}
