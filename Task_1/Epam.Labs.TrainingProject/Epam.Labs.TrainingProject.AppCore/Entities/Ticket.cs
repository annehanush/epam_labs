﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;

namespace Epam.Labs.TrainingProject.AppCore.Entities
{
    public enum TicketType
    {
        Standart, Popular, Important
    }

    /// <summary>
    /// Ticket (id, event, price, seller, ticket type, ticket status).
    /// </summary>
    public class Ticket
    {
        public Guid Id { get; set; }

        [Required]
        public decimal Price { get; set; }
        public TicketType? TicketType { get; set; }

        public Guid SellerId { get; set; }
        public User Seller { get; set; }

        public Guid EventId { get; set; }
        public Event Event { get; set; }

        public Guid StatusId { get; set; }
        public Status Status { get; set; }

        public ICollection<Order> Orders { get; set; }
    }
}
