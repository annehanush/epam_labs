﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;

namespace Epam.Labs.TrainingProject.AppCore.Entities
{
    /// <summary>
    /// Venue (id, name, address, city).
    /// </summary>
    public class Venue
    {
        public Guid Id { get; set; }

        [Required]
        public string Name { get; set; }

        [Required]
        public string Address { get; set; }

        public Guid CityId { get; set; }
        public City City { get; set; }

        public ICollection<Event> Events { get; set; }
    }
}
