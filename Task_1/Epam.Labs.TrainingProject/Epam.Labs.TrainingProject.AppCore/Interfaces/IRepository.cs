﻿using System;
using System.Collections.Generic;
using System.Linq.Expressions;

namespace Epam.Labs.TrainingProject.AppCore.Interfaces
{
    public interface IRepository<T> where T : class
    {
        IEnumerable<T> GetAll();
        void Create(T item);
        T Get(Guid id);
        void Update(T item);
        void Remove(T item);
        IEnumerable<T> GetWithInclude(params Expression<Func<T, object>>[] includeProperties);
    }
}
