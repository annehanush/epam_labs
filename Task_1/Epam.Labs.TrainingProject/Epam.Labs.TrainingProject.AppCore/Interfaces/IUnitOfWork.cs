﻿using Epam.Labs.TrainingProject.AppCore.Entities;

namespace Epam.Labs.TrainingProject.AppCore.Interfaces
{
    public interface IUnitOfWork
    {
        IRepository<City> Cities { get; }
        IRepository<Category> Categories { get; }
        IRepository<Culture> Cultures { get; }
        IRepository<Venue> Venues { get; }
        IRepository<User> Users { get; }
        IRepository<Event> Events { get; }
        IRepository<Ticket> Tickets { get; }
        IRepository<Role> Roles { get; }
        IRepository<Status> Statuses { get; }
        IRepository<Order> Orders { get; }
        IRepository<AuthenticationData> AuthenticationDatas { get; }
    }
}
