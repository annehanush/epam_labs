﻿using System;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Infrastructure;
using Microsoft.EntityFrameworkCore.Metadata;
using Microsoft.EntityFrameworkCore.Migrations;
using Epam.Labs.TrainingProject.AppCore.Data;

namespace Epam.Labs.TrainingProject.AppCore.Migrations
{
    [DbContext(typeof(AppDataContext))]
    partial class AppDataContextModelSnapshot : ModelSnapshot
    {
        protected override void BuildModel(ModelBuilder modelBuilder)
        {
            modelBuilder
                .HasAnnotation("ProductVersion", "1.1.2")
                .HasAnnotation("SqlServer:ValueGenerationStrategy", SqlServerValueGenerationStrategy.IdentityColumn);

            modelBuilder.Entity("Epam.Labs.TrainingProject.AppCore.Entities.AuthenticationData", b =>
                {
                    b.Property<Guid>("Id")
                        .ValueGeneratedOnAdd();

                    b.Property<string>("Email")
                        .IsRequired();

                    b.Property<string>("Password")
                        .IsRequired();

                    b.Property<Guid>("RoleId");

                    b.Property<Guid>("UserId");

                    b.Property<string>("Username")
                        .IsRequired();

                    b.HasKey("Id");

                    b.HasIndex("RoleId");

                    b.HasIndex("UserId");

                    b.ToTable("AuthenticationData");
                });

            modelBuilder.Entity("Epam.Labs.TrainingProject.AppCore.Entities.Category", b =>
                {
                    b.Property<Guid>("Id")
                        .ValueGeneratedOnAdd();

                    b.Property<string>("Name")
                        .IsRequired();

                    b.HasKey("Id");

                    b.ToTable("Category");
                });

            modelBuilder.Entity("Epam.Labs.TrainingProject.AppCore.Entities.City", b =>
                {
                    b.Property<Guid>("Id")
                        .ValueGeneratedOnAdd();

                    b.Property<string>("Name")
                        .IsRequired();

                    b.HasKey("Id");

                    b.ToTable("City");
                });

            modelBuilder.Entity("Epam.Labs.TrainingProject.AppCore.Entities.Culture", b =>
                {
                    b.Property<Guid>("Id")
                        .ValueGeneratedOnAdd();

                    b.Property<string>("Name")
                        .IsRequired();

                    b.HasKey("Id");

                    b.ToTable("Culture");
                });

            modelBuilder.Entity("Epam.Labs.TrainingProject.AppCore.Entities.Event", b =>
                {
                    b.Property<Guid>("Id")
                        .ValueGeneratedOnAdd();

                    b.Property<string>("Banner")
                        .IsRequired();

                    b.Property<Guid>("CategoryId");

                    b.Property<DateTime>("Date");

                    b.Property<string>("Description");

                    b.Property<string>("Name")
                        .IsRequired();

                    b.Property<Guid>("VenueId");

                    b.HasKey("Id");

                    b.HasIndex("CategoryId");

                    b.HasIndex("VenueId");

                    b.ToTable("Event");
                });

            modelBuilder.Entity("Epam.Labs.TrainingProject.AppCore.Entities.Order", b =>
                {
                    b.Property<Guid>("Id")
                        .ValueGeneratedOnAdd();

                    b.Property<Guid>("BuyerId");

                    b.Property<DateTime>("Date");

                    b.Property<string>("RejectionComment");

                    b.Property<Guid>("StatusId");

                    b.Property<Guid?>("TicketId");

                    b.Property<string>("TrackNO")
                        .IsRequired();

                    b.HasKey("Id");

                    b.HasIndex("BuyerId");

                    b.HasIndex("StatusId");

                    b.HasIndex("TicketId");

                    b.ToTable("Order");
                });

            modelBuilder.Entity("Epam.Labs.TrainingProject.AppCore.Entities.Role", b =>
                {
                    b.Property<Guid>("Id")
                        .ValueGeneratedOnAdd();

                    b.Property<string>("Name")
                        .IsRequired();

                    b.HasKey("Id");

                    b.ToTable("Role");
                });

            modelBuilder.Entity("Epam.Labs.TrainingProject.AppCore.Entities.Status", b =>
                {
                    b.Property<Guid>("Id")
                        .ValueGeneratedOnAdd();

                    b.Property<string>("Name")
                        .IsRequired();

                    b.HasKey("Id");

                    b.ToTable("Status");
                });

            modelBuilder.Entity("Epam.Labs.TrainingProject.AppCore.Entities.Ticket", b =>
                {
                    b.Property<Guid>("Id")
                        .ValueGeneratedOnAdd();

                    b.Property<Guid>("EventId");

                    b.Property<decimal>("Price");

                    b.Property<Guid>("SellerId");

                    b.Property<Guid>("StatusId");

                    b.Property<int?>("TicketType");

                    b.HasKey("Id");

                    b.HasIndex("EventId");

                    b.HasIndex("SellerId");

                    b.HasIndex("StatusId");

                    b.ToTable("Ticket");
                });

            modelBuilder.Entity("Epam.Labs.TrainingProject.AppCore.Entities.User", b =>
                {
                    b.Property<Guid>("Id")
                        .ValueGeneratedOnAdd();

                    b.Property<string>("Address");

                    b.Property<Guid>("CultureId");

                    b.Property<string>("FirstName")
                        .IsRequired();

                    b.Property<string>("LastName")
                        .IsRequired();

                    b.Property<string>("Phone");

                    b.HasKey("Id");

                    b.HasIndex("CultureId");

                    b.ToTable("User");
                });

            modelBuilder.Entity("Epam.Labs.TrainingProject.AppCore.Entities.Venue", b =>
                {
                    b.Property<Guid>("Id")
                        .ValueGeneratedOnAdd();

                    b.Property<string>("Address")
                        .IsRequired();

                    b.Property<Guid>("CityId");

                    b.Property<string>("Name")
                        .IsRequired();

                    b.HasKey("Id");

                    b.HasIndex("CityId");

                    b.ToTable("Venue");
                });

            modelBuilder.Entity("Epam.Labs.TrainingProject.AppCore.Entities.AuthenticationData", b =>
                {
                    b.HasOne("Epam.Labs.TrainingProject.AppCore.Entities.Role", "Role")
                        .WithMany("AuthenticationDatas")
                        .HasForeignKey("RoleId")
                        .OnDelete(DeleteBehavior.Cascade);

                    b.HasOne("Epam.Labs.TrainingProject.AppCore.Entities.User", "User")
                        .WithMany()
                        .HasForeignKey("UserId")
                        .OnDelete(DeleteBehavior.Cascade);
                });

            modelBuilder.Entity("Epam.Labs.TrainingProject.AppCore.Entities.Event", b =>
                {
                    b.HasOne("Epam.Labs.TrainingProject.AppCore.Entities.Category", "Category")
                        .WithMany("Events")
                        .HasForeignKey("CategoryId")
                        .OnDelete(DeleteBehavior.Cascade);

                    b.HasOne("Epam.Labs.TrainingProject.AppCore.Entities.Venue", "Venue")
                        .WithMany("Events")
                        .HasForeignKey("VenueId")
                        .OnDelete(DeleteBehavior.Cascade);
                });

            modelBuilder.Entity("Epam.Labs.TrainingProject.AppCore.Entities.Order", b =>
                {
                    b.HasOne("Epam.Labs.TrainingProject.AppCore.Entities.User", "Buyer")
                        .WithMany("Orders")
                        .HasForeignKey("BuyerId")
                        .OnDelete(DeleteBehavior.Cascade);

                    b.HasOne("Epam.Labs.TrainingProject.AppCore.Entities.Status", "Status")
                        .WithMany("Orders")
                        .HasForeignKey("StatusId")
                        .OnDelete(DeleteBehavior.Cascade);

                    b.HasOne("Epam.Labs.TrainingProject.AppCore.Entities.Ticket", "Ticket")
                        .WithMany("Orders")
                        .HasForeignKey("TicketId");
                });

            modelBuilder.Entity("Epam.Labs.TrainingProject.AppCore.Entities.Ticket", b =>
                {
                    b.HasOne("Epam.Labs.TrainingProject.AppCore.Entities.Event", "Event")
                        .WithMany("Tickets")
                        .HasForeignKey("EventId")
                        .OnDelete(DeleteBehavior.Cascade);

                    b.HasOne("Epam.Labs.TrainingProject.AppCore.Entities.User", "Seller")
                        .WithMany("Tickets")
                        .HasForeignKey("SellerId")
                        .OnDelete(DeleteBehavior.Cascade);

                    b.HasOne("Epam.Labs.TrainingProject.AppCore.Entities.Status", "Status")
                        .WithMany("Tickets")
                        .HasForeignKey("StatusId")
                        .OnDelete(DeleteBehavior.Cascade);
                });

            modelBuilder.Entity("Epam.Labs.TrainingProject.AppCore.Entities.User", b =>
                {
                    b.HasOne("Epam.Labs.TrainingProject.AppCore.Entities.Culture", "Culture")
                        .WithMany("Users")
                        .HasForeignKey("CultureId")
                        .OnDelete(DeleteBehavior.Cascade);
                });

            modelBuilder.Entity("Epam.Labs.TrainingProject.AppCore.Entities.Venue", b =>
                {
                    b.HasOne("Epam.Labs.TrainingProject.AppCore.Entities.City", "City")
                        .WithMany("Venues")
                        .HasForeignKey("CityId")
                        .OnDelete(DeleteBehavior.Cascade);
                });
        }
    }
}
