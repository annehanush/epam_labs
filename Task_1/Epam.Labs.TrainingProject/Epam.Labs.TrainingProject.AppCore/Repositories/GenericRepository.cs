﻿using Epam.Labs.TrainingProject.AppCore.Data;
using Epam.Labs.TrainingProject.AppCore.Interfaces;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;

namespace Epam.Labs.TrainingProject.AppCore.Repositories
{
    public class GenericRepository<T> : IRepository<T> where T : class
    {
        private AppDataContext db;
        DbSet<T> _dbSet;

        public GenericRepository(AppDataContext context)
        {
            db = context;
            _dbSet = context.Set<T>();
        }

        public void Create(T item)
        {
            _dbSet.Add(item);
            db.SaveChanges();
            db.Entry(item).State = EntityState.Detached;
        }

        public T Get(Guid id)
        {
            return _dbSet.Find(id);
        }

        public IEnumerable<T> GetAll()
        {
            return _dbSet.AsNoTracking().ToList();
        }

        public void Update(T item)
        {
            db.Entry(item).State = EntityState.Modified;
            db.SaveChanges();
            db.Entry(item).State = EntityState.Detached;
        }
        public void Remove(T item)
        {
            db.Entry(item).State = EntityState.Deleted;
            db.SaveChanges();
        }

        public IEnumerable<T> GetWithInclude(params Expression<Func<T, object>>[] includeProperties)
        {
            return Include(includeProperties).ToList();
        }

        public IEnumerable<T> GetWithInclude(Func<T, bool> predicate,
            params Expression<Func<T, object>>[] includeProperties)
        {
            var query = Include(includeProperties);
            return query.Where(predicate).ToList();
        }

        private IQueryable<T> Include(params Expression<Func<T, object>>[] includeProperties)
        {
            IQueryable<T> query = _dbSet.AsNoTracking();
            return includeProperties
                .Aggregate(query, (current, includeProperty) => current.Include(includeProperty));
        }
    }
}
