﻿using Epam.Labs.TrainingProject.AppCore.Data;
using Epam.Labs.TrainingProject.AppCore.Entities;
using Epam.Labs.TrainingProject.AppCore.Interfaces;

namespace Epam.Labs.TrainingProject.AppCore.Repositories
{
    public class UnitOfWork : IUnitOfWork
    {
        private AppDataContext db;

        private GenericRepository<City> cityRepository;
        private GenericRepository<Culture> cultureRepository;
        private GenericRepository<Category> categoryRepository;
        private GenericRepository<Venue> venueRepository;
        private GenericRepository<User> userRepository;
        private GenericRepository<Event> eventRepository;
        private GenericRepository<Ticket> ticketRepository;
        private GenericRepository<Role> roleRepository;
        private GenericRepository<Status> statusRepository;
        private GenericRepository<Order> orderRepository;
        private GenericRepository<AuthenticationData> authenticationRepository;

        public UnitOfWork(AppDataContext context)
        {
            db = context;
        }

        public IRepository<City> Cities
        {
            get
            {
                if (cityRepository == null)
                {
                    cityRepository = new GenericRepository<City>(db);
                }
                return cityRepository;
            }
        }

        public IRepository<Culture> Cultures
        {
            get
            {
                if (cultureRepository == null)
                {
                    cultureRepository = new GenericRepository<Culture>(db);
                }
                return cultureRepository;
            }
        }

        public IRepository<Category> Categories
        {
            get
            {
                if (categoryRepository == null)
                {
                    categoryRepository = new GenericRepository<Category>(db);
                }
                return categoryRepository;
            }
        }

        public IRepository<Venue> Venues
        {
            get
            {
                if (venueRepository == null)
                {
                    venueRepository = new GenericRepository<Venue>(db);
                }
                return venueRepository;
            }
        }

        public IRepository<Role> Roles
        {
            get
            {
                if (roleRepository == null)
                {
                    roleRepository = new GenericRepository<Role>(db);
                }
                return roleRepository;
            }
        }

        public IRepository<User> Users
        {
            get
            {
                if (userRepository == null)
                {
                    userRepository = new GenericRepository<User>(db);
                }
                return userRepository;
            }
        }

        public IRepository<Event> Events
        {
            get
            {
                if (eventRepository == null)
                {
                    eventRepository = new GenericRepository<Event>(db);
                }
                return eventRepository;
            }
        }

        public IRepository<Ticket> Tickets
        {
            get
            {
                if (ticketRepository == null)
                {
                    ticketRepository = new GenericRepository<Ticket>(db);
                }
                return ticketRepository;
            }
        }

        public IRepository<Status> Statuses
        {
            get
            {
                if (statusRepository == null)
                {
                    statusRepository = new GenericRepository<Status>(db);
                }
                return statusRepository;
            }
        }

        public IRepository<Order> Orders
        {
            get
            {
                if (orderRepository == null)
                {
                    orderRepository = new GenericRepository<Order>(db);
                }
                return orderRepository;
            }
        }

        public IRepository<AuthenticationData> AuthenticationDatas
        {
            get
            {
                if (authenticationRepository == null)
                {
                    authenticationRepository = new GenericRepository<AuthenticationData>(db);
                }
                return authenticationRepository;
            }
        }
    }
}
