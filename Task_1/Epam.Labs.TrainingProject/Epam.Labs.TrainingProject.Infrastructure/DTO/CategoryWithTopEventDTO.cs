﻿using Epam.Labs.TrainingProject.AppCore.Entities;

namespace Epam.Labs.TrainingProject.Infrastructure.DTO
{
    public class CategoryWithTopEventDTO
    {
        public string CategoryName { get; set; }
        public Event TopEventOfCategory { get; set; }
    }
}
