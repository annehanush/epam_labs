﻿using System;

namespace Epam.Labs.TrainingProject.Infrastructure.DTO
{
    public class EditUserInfoDTO
    {
        public string FirstName { get; set; }
        public string LastName { get; set; }
        public string Email { get; set; }
        public string Address { get; set; }
        public string Phone { get; set; }
        public Guid CultureId { get; set; }
    }
}
