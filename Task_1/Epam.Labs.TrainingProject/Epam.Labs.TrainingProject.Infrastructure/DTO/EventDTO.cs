﻿using Epam.Labs.TrainingProject.AppCore.Entities;
using System;

namespace Epam.Labs.TrainingProject.Infrastructure.DTO
{
    public class EventDTO
    {
        public Guid Id { get; set; }
        public string Name { get; set; }
        public DateTime Date { get; set; }
        public string Banner { get; set; }
        public Venue Venue { get; set; }
        public Category Category { get; set; }
        public string Description { get; set; }
        public decimal MinTicketPrice { get; set; }
        public decimal MaxTicketPrice { get; set; }
    }
}
