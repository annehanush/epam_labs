﻿using Epam.Labs.TrainingProject.AppCore.Interfaces;
using Epam.Labs.TrainingProject.Infrastructure.Interfaces;
using Epam.Labs.TrainingProject.AppCore.Entities;
using System;
using System.Linq;
using System.Collections;
using System.Collections.Generic;

namespace Epam.Labs.TrainingProject.Infrastructure.Facades
{
    public class AccountFacade : IAccountFacade
    {
        IUnitOfWork _unitOfWork;

        public AccountFacade(IUnitOfWork unitOfWork)
        {
            _unitOfWork = unitOfWork;
        }

        public AuthenticationData GetAuthenticationByUsername(string username)
        {
            return _unitOfWork.AuthenticationDatas.GetWithInclude(a => a.Role, a => a.User, a => a.User.Culture).FirstOrDefault(u => u.Username == username);
        }

        public AuthenticationData GetAuthenticationByUsernameAmdPassword(string username, string password)
        {
            return _unitOfWork.AuthenticationDatas.GetWithInclude(a => a.User, a => a.Role, a => a.User.Culture).FirstOrDefault(u => u.Username == username && u.Password == password);
        }

        public Role GetRoleById(Guid id)
        {
            return _unitOfWork.Roles.Get(id);
        }

        public Role GetRoleByName(string name)
        {
            return _unitOfWork.Roles.GetAll().FirstOrDefault(r => r.Name == name);
        }

        public int GetUserCartItemsAmount(string username)
        {
            var authData = GetAuthenticationByUsername(username);

            return _unitOfWork.Orders.GetAll().Where(o => o.BuyerId == authData.UserId && o.StatusId == Guid.Parse("96e402e9-38d8-4c79-a6a4-190d307c86ea")).Count();
        }

        public IEnumerable<AuthenticationData> GetAllAppUsers(string currentUser)
        {
            return _unitOfWork.AuthenticationDatas.GetWithInclude(a => a.Role, a => a.User).Where(a => a.Username != currentUser).ToList();
        }
    }
}
