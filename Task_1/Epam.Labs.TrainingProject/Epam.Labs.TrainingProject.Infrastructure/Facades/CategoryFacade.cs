﻿using Epam.Labs.TrainingProject.AppCore.Entities;
using Epam.Labs.TrainingProject.AppCore.Interfaces;
using Epam.Labs.TrainingProject.Infrastructure.Interfaces;
using System.Collections.Generic;
using Epam.Labs.TrainingProject.Infrastructure.DTO;
using System.Linq;
using System;

namespace Epam.Labs.TrainingProject.Infrastructure.Facades
{
    public class CategoryFacade : ICategoryFacade
    {
        IUnitOfWork _unitOfWork;

        public CategoryFacade(IUnitOfWork unitOfWork)
        {
            _unitOfWork = unitOfWork;
        }

        public IEnumerable<Category> GetAllCategories()
        {
            return _unitOfWork.Categories.GetAll();
        }

        /// <summary>
        /// Get specific category by its id.
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        public Category GetCategory(Guid id)
        {
            return _unitOfWork.Categories.Get(id);
        }

        public IEnumerable<Category> GetAllCategoriesWhereEventsExist()
        {
            return _unitOfWork.Categories.GetWithInclude(c => c.Events).Where(c => c.Events.Count > 0);
        }

        /// <summary>
        /// Get all categories with top event in each.
        /// </summary>
        /// <returns></returns>
        public IEnumerable<CategoryWithTopEventDTO> GetCategoriesWithTopEvents()
        {
            List<CategoryWithTopEventDTO> _results = new List<CategoryWithTopEventDTO>();

            foreach (var category in _unitOfWork.Categories.GetWithInclude(c => c.Events))
            {
                CategoryWithTopEventDTO tmp = new CategoryWithTopEventDTO();
                tmp.CategoryName = category.Name;

                if (category.Events.Count > 0)
                {
                    tmp.TopEventOfCategory = category.Events.OrderBy(e => e.Date).First();
                    _results.Add(tmp);
                }
            }

            return _results;
        }
    }
}
