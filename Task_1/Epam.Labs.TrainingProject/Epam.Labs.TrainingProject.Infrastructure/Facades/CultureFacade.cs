﻿using Epam.Labs.TrainingProject.Infrastructure.Interfaces;
using System.Collections.Generic;
using Epam.Labs.TrainingProject.AppCore.Entities;
using Epam.Labs.TrainingProject.AppCore.Interfaces;
using System.Linq;

namespace Epam.Labs.TrainingProject.Infrastructure.Facades
{
    public class CultureFacade : ICultureFacade
    {
        IUnitOfWork _unitOfWork;

        public CultureFacade(IUnitOfWork unitOfWork)
        {
            _unitOfWork = unitOfWork;
        }

        public IEnumerable<Culture> GetAllCultures()
        {
            return _unitOfWork.Cultures.GetAll();
        }

        public Culture GetCultureByName(string name)
        {
            return _unitOfWork.Cultures.GetAll().FirstOrDefault(c => c.Name == name);
        }

        public List<string> GetAllCulturesNames()
        {
            List<string> cultures = new List<string>();

            foreach (var culture in _unitOfWork.Cultures.GetAll())
            {
                cultures.Add(culture.Name);
            }

            return cultures;
        }
    }
}
