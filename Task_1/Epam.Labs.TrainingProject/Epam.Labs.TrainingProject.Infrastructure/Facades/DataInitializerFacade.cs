﻿using Epam.Labs.TrainingProject.AppCore.Entities;
using Epam.Labs.TrainingProject.AppCore.Interfaces;
using Epam.Labs.TrainingProject.Infrastructure.Interfaces;
using System;
using System.Globalization;
using System.Linq;

namespace Epam.Labs.TrainingProject.Infrastructure.Facades
{
    public class DataInitializerFacade : IDataInitializerFacade
    {

        IUnitOfWork _unitOfWOrk;

        public DataInitializerFacade(IUnitOfWork unitOfWork)
        {
            _unitOfWOrk = unitOfWork;
        }

        public void Initialize()
        {
            // Look for any cities.
            if (_unitOfWOrk.Cities.GetAll().Any())
            {
                return;   // DB has been seeded
            }

            var _cities = new City[]
            {
                    new City { Id = Guid.Parse("2dd3ada1-1d74-4cdc-92eb-1f6d2f2abaf1"), Name = "Seattle" },
                    new City { Id = Guid.Parse("5d6133cd-d16b-4702-914b-5c0bd36e7500"), Name = "Los-Angeles" },
                    new City { Id = Guid.Parse("fe5fe22d-35aa-426e-9d0d-0adb7a0c9a84"), Name = "Chicago" },
                    new City { Id = Guid.Parse("3f811f11-5c88-4b0e-a645-577e874b09d2"), Name = "Boston" },
                    new City { Id = Guid.Parse("4f52d6a1-f71b-40a1-837c-9788f1663fd8"), Name = "Atlanta" },
                    new City { Id = Guid.Parse("d8d7022b-3146-4eef-a6f2-944bcfd8530c"), Name = "Phoenix" }
            };
            foreach (var city in _cities)
            {
                _unitOfWOrk.Cities.Create(city);
            }


            var _roles = new Role[]
            {
                    new Role { Id = Guid.Parse("535c52ba-a706-4d8e-91c1-17b748ebdda7"), Name = "admin" },
                    new Role { Id = Guid.Parse("157964cf-6b71-4f72-a7c3-b243158fec68"), Name = "user" }
            };
            foreach (var role in _roles)
            {
                _unitOfWOrk.Roles.Create(role);
            }

            var _statuses = new Status[]
            {
                    new Status { Id = Guid.Parse("97398191-cc77-4d97-b747-71094d0fdff0"), Name = "Waiting for seller Confirmation" },
                    new Status { Id = Guid.Parse("83a182ef-be1c-4e8b-ac29-c79e23b378e5"), Name = "Selling" },
                    new Status { Id = Guid.Parse("d4e8d141-70be-4216-9967-f8a6a31cfcc2"), Name = "Sold" },
                    new Status { Id = Guid.Parse("680d1cad-881a-4791-b684-4749bcfe8879"), Name = "Confirmed" },
                    new Status { Id = Guid.Parse("28770feb-a30f-49d9-bd3f-d82024cadc6a"), Name = "Rejected" },
                    new Status { Id = Guid.Parse("96e402e9-38d8-4c79-a6a4-190d307c86ea"), Name = "Waiting for buyer Confirmation" }
            };
            foreach (var status in _statuses)
            {
                _unitOfWOrk.Statuses.Create(status);
            }

            var _cultures = new Culture[]
            {
                    new Culture { Id = Guid.Parse("aba33d79-8f0d-4b34-9731-450671a81561"), Name = "en" },
                    new Culture { Id = Guid.Parse("4db3b1e1-86da-4e68-beed-e3b5a4aad68b"), Name = "ru" },
                    new Culture { Id = Guid.Parse("0940b812-a9f5-4d44-895c-dd901136908c"), Name = "be" }
            };
            foreach (var culture in _cultures)
            {
                _unitOfWOrk.Cultures.Create(culture);
            }

            var _users = new User[]
            {
                    new User { Id = Guid.Parse("e648b50d-9c71-4425-93e9-f9cc7617d00c"), FirstName = "Anne", LastName = "Hanush", Address = "", CultureId = Guid.Parse("aba33d79-8f0d-4b34-9731-450671a81561"), Phone = "" },
                    new User { Id = Guid.Parse("73196af2-90f8-479a-8b33-36411128b71e"), FirstName = "Alex", LastName = "Gray", Address = "", CultureId = Guid.Parse("4db3b1e1-86da-4e68-beed-e3b5a4aad68b"), Phone = "" },
                    new User { Id = Guid.Parse("07107e6a-b384-4778-ae37-617616537c7c"), FirstName = "Sam", LastName = "Smith", Address = "", CultureId = Guid.Parse("0940b812-a9f5-4d44-895c-dd901136908c"), Phone = "" },
                    new User { Id = Guid.Parse("071619af-4cf9-4531-bd77-2502736b4000"), FirstName = "Thomas", LastName = "Morris", Address = "", CultureId = Guid.Parse("aba33d79-8f0d-4b34-9731-450671a81561"), Phone = "" }
            };
            foreach (var user in _users)
            {
                _unitOfWOrk.Users.Create(user);
            }

            var _authenications = new AuthenticationData[]
            {
                    new AuthenticationData { Username = "Admin", Email = "anne.hanush97@gmail.com", Password = "Admin", RoleId = Guid.Parse("535c52ba-a706-4d8e-91c1-17b748ebdda7"), UserId = Guid.Parse("e648b50d-9c71-4425-93e9-f9cc7617d00c") },
                    new AuthenticationData { Username = "User", Email = "alex@gmail.com", Password = "User", RoleId = Guid.Parse("157964cf-6b71-4f72-a7c3-b243158fec68"), UserId = Guid.Parse("73196af2-90f8-479a-8b33-36411128b71e") },
                    new AuthenticationData { Username = "SamS", Email = "sam@gmail.com", Password = "111111", RoleId = Guid.Parse("157964cf-6b71-4f72-a7c3-b243158fec68"), UserId = Guid.Parse("07107e6a-b384-4778-ae37-617616537c7c") },
                    new AuthenticationData { Username = "ThomasM", Email = "thomas@gmail.com", Password = "111111", RoleId = Guid.Parse("157964cf-6b71-4f72-a7c3-b243158fec68"), UserId = Guid.Parse("071619af-4cf9-4531-bd77-2502736b4000") }
            };
            foreach (var auth in _authenications)
            {
                _unitOfWOrk.AuthenticationDatas.Create(auth);
            }

            var _venues = new Venue[]
            {
                    new Venue { Id = Guid.Parse("ae85562c-116c-4f14-b3cb-edb89b5c649c"), Name = "Tacoma Musical Playhouse", Address = "7116 6th Ave", CityId = Guid.Parse("2dd3ada1-1d74-4cdc-92eb-1f6d2f2abaf1") },
                    new Venue { Id = Guid.Parse("235bdfd5-140f-477e-b515-e02e7151debc"), Name = "The Triple Door", Address = "216 Union St", CityId = Guid.Parse("2dd3ada1-1d74-4cdc-92eb-1f6d2f2abaf1") },
                    new Venue { Id = Guid.Parse("45f036e9-df98-448a-87f8-96ce0fe38b7c"), Name = "Hollywood Bowl", Address = "2301 Highland Ave", CityId = Guid.Parse("5d6133cd-d16b-4702-914b-5c0bd36e7500") },
                    new Venue { Id = Guid.Parse("947df91f-9e08-4bda-8a64-ff58f36afefa"), Name = "Rainbow Lagoon Park", Address = "400 Shoreline Village Dr", CityId = Guid.Parse("5d6133cd-d16b-4702-914b-5c0bd36e7500") },
                    new Venue { Id = Guid.Parse("bc8e003c-a064-44cd-9963-b011c1757600"), Name = "Cadillac Palace Theatre", Address = "151 W Randolph St", CityId = Guid.Parse("fe5fe22d-35aa-426e-9d0d-0adb7a0c9a84") },
                    new Venue { Id = Guid.Parse("bcab8f9c-fc5c-462c-ad90-58128bcaf954"), Name = "Spirit of Chicago", Address = "600 E Grand Ave", CityId = Guid.Parse("fe5fe22d-35aa-426e-9d0d-0adb7a0c9a84") },
                    new Venue { Id = Guid.Parse("a1596c8b-4e7b-4bed-9f1b-3915f5f1d5ed"), Name = "Wheelock Family Theatre", Address = "180 Riverway", CityId = Guid.Parse("3f811f11-5c88-4b0e-a645-577e874b09d2") },
                    new Venue { Id = Guid.Parse("a487c2f8-568c-42c9-8d86-4c1e05c06b04"), Name = "Massachusetts Bay Lines Vessels", Address = "60 Rowes Wharf", CityId = Guid.Parse("3f811f11-5c88-4b0e-a645-577e874b09d2") },
                    new Venue { Id = Guid.Parse("d5ab42c2-ec4a-4a10-bfb7-ead712d236ca"), Name = "Fox Theatre", Address = "660 West Peachtree St NW", CityId = Guid.Parse("4f52d6a1-f71b-40a1-837c-9788f1663fd8") },
                    new Venue { Id = Guid.Parse("e40be5c0-6901-4159-8e7f-ae35e7adac98"), Name = "City Winery Atlanta", Address = "650 North Avenue NE #201", CityId = Guid.Parse("4f52d6a1-f71b-40a1-837c-9788f1663fd8") },
                    new Venue { Id = Guid.Parse("33a306ab-6e96-45aa-8fb3-753a006c090c"), Name = "SEA LIFE Arizona", Address = "5000 S Arizona Mills Cir #145", CityId = Guid.Parse("d8d7022b-3146-4eef-a6f2-944bcfd8530c") },
                    new Venue { Id = Guid.Parse("9877a0d6-2f7a-4d13-8a5a-fe659102521a"), Name = "Phoenix Theatre - Mainstage Theatre", Address = "Charing Cross Rd", CityId = Guid.Parse("d8d7022b-3146-4eef-a6f2-944bcfd8530c") }
            };
            foreach (var venue in _venues)
            {
                _unitOfWOrk.Venues.Create(venue);
            }

            var _categories = new Category[]
            {
                    new Category { Id = Guid.Parse("abb22b0e-656a-47af-8d28-3394cdf50d07"), Name = "Art" },
                    new Category { Id = Guid.Parse("4aeda676-3636-4bb7-aa52-c6da4f89419f"), Name = "Politics" },
                    new Category { Id = Guid.Parse("0b231531-7206-43d2-aa30-97a9b1b1c94a"), Name = "Business" },
                    new Category { Id = Guid.Parse("b19cdda5-c7a3-46f3-b8aa-3599576696d9"), Name = "Music" },
                    new Category { Id = Guid.Parse("da4f4373-e0cd-448c-b517-4488ceeadea5"), Name = "Sports" },
                    new Category { Id = Guid.Parse("e2bca67e-811a-4187-afcf-2366c8e5fc25"), Name = "Tech" },
                    new Category { Id = Guid.Parse("7fd85510-368c-45d1-b03d-0daf4083e530"), Name = "Life style" },
                    new Category { Id = Guid.Parse("fc694ff8-78be-4c2b-83ef-d0cf55020924"), Name = "Culture" },
                    new Category { Id = Guid.Parse("b1461260-149f-43ee-815b-7ee6e9af716f"), Name = "Celebrations" }
            };
            foreach (var category in _categories)
            {
                _unitOfWOrk.Categories.Create(category);
            }

            var _events = new Event[]
            {
                    new Event { Id = Guid.Parse("7f38e8bd-d211-49db-955d-c5d9a04d6c3d"), Name = "Swing Reunion Orchestra", Date = DateTime.ParseExact("2017-12-01", "yyyy-MM-dd", CultureInfo.InvariantCulture, DateTimeStyles.None), Banner = "http://res.cloudinary.com/dby4pjuo8/image/upload/w_700,h_400/Re-ticket%20system/Events/Swing_Reunion_Orchestra.jpg", VenueId = Guid.Parse("ae85562c-116c-4f14-b3cb-edb89b5c649c"), Description = "", CategoryId = Guid.Parse("b19cdda5-c7a3-46f3-b8aa-3599576696d9") },
                    new Event { Id = Guid.Parse("8df34096-ac54-4936-91a0-646983840c57"), Name = "Maxwell", Date = DateTime.ParseExact("2017-08-18", "yyyy-MM-dd", CultureInfo.InvariantCulture, DateTimeStyles.None), Banner = "http://res.cloudinary.com/dby4pjuo8/image/upload/w_700,h_400/Re-ticket%20system/Events/Maxwell.jpg", VenueId = Guid.Parse("ae85562c-116c-4f14-b3cb-edb89b5c649c"), Description = "", CategoryId = Guid.Parse("abb22b0e-656a-47af-8d28-3394cdf50d07") },
                    new Event { Id = Guid.Parse("d284d394-9af7-42fc-ab16-de998ac2bac8"), Name = "The Triple Door", Date = DateTime.ParseExact("2017-09-14", "yyyy-MM-dd", CultureInfo.InvariantCulture, DateTimeStyles.None), Banner = "http://res.cloudinary.com/dby4pjuo8/image/upload/w_700,h_400/Re-ticket%20system/Events/The_Triple_Door.jpg", VenueId = Guid.Parse("235bdfd5-140f-477e-b515-e02e7151debc"), Description = "", CategoryId = Guid.Parse("b19cdda5-c7a3-46f3-b8aa-3599576696d9") },
                    new Event { Id = Guid.Parse("131ae463-8303-46e1-8c5c-82ab4c948083"), Name = "The Yardbirds", Date = DateTime.ParseExact("2017-09-05", "yyyy-MM-dd", CultureInfo.InvariantCulture, DateTimeStyles.None), Banner = "http://res.cloudinary.com/dby4pjuo8/image/upload/w_700,h_400/Re-ticket%20system/Events/The_Yardbirds.jpg", VenueId = Guid.Parse("235bdfd5-140f-477e-b515-e02e7151debc"), Description = "", CategoryId = Guid.Parse("b19cdda5-c7a3-46f3-b8aa-3599576696d9") },
                    new Event { Id = Guid.Parse("1c90a9d5-6712-4d8f-9f55-619f5a6f2093"), Name = "Long Beach BBQ Festival", Date = DateTime.ParseExact("2017-10-03", "yyyy-MM-dd", CultureInfo.InvariantCulture, DateTimeStyles.None), Banner = "http://res.cloudinary.com/dby4pjuo8/image/upload/w_700,h_400/Re-ticket%20system/Events/Long_Beach_BBQ_Festival.jpg", VenueId = Guid.Parse("947df91f-9e08-4bda-8a64-ff58f36afefa"), Description = "", CategoryId = Guid.Parse("7fd85510-368c-45d1-b03d-0daf4083e530") },
                    new Event { Id = Guid.Parse("650148f5-9802-4000-81c3-cad1ef5576e6"), Name = "Violinist Plays John Williams", Date = DateTime.ParseExact("2017-10-15", "yyyy-MM-dd", CultureInfo.InvariantCulture, DateTimeStyles.None), Banner = "http://res.cloudinary.com/dby4pjuo8/image/upload/w_700,h_400/Re-ticket%20system/Events/Violinist_Gil_Shaham_Plays_John_Williams.jpg", VenueId = Guid.Parse("45f036e9-df98-448a-87f8-96ce0fe38b7c"), Description = "", CategoryId = Guid.Parse("b19cdda5-c7a3-46f3-b8aa-3599576696d9") },
                    new Event { Id = Guid.Parse("3a363d90-e7e2-4282-b510-491329851fc6"), Name = "LA Phil: All-Vivaldi", Date = DateTime.ParseExact("2017-09-11", "yyyy-MM-dd", CultureInfo.InvariantCulture, DateTimeStyles.None), Banner = "http://res.cloudinary.com/dby4pjuo8/image/upload/w_700,h_400/Re-ticket%20system/Events/LA_Phil_All-Vivaldi.jpg", VenueId = Guid.Parse("45f036e9-df98-448a-87f8-96ce0fe38b7c"), Description = "", CategoryId = Guid.Parse("b19cdda5-c7a3-46f3-b8aa-3599576696d9") },
                    new Event { Id = Guid.Parse("106384b8-4aa0-4652-b3e4-30a5c9f17350"), Name = "LA Phil: Tchaikovsky & Liszt", Date = DateTime.ParseExact("2017-10-01", "yyyy-MM-dd", CultureInfo.InvariantCulture, DateTimeStyles.None), Banner = "http://res.cloudinary.com/dby4pjuo8/image/upload/w_700,h_400/Re-ticket%20system/Events/LA_Phil_Tchaikovsky_Liszt.jpg", VenueId = Guid.Parse("45f036e9-df98-448a-87f8-96ce0fe38b7c"), Description = "", CategoryId = Guid.Parse("b19cdda5-c7a3-46f3-b8aa-3599576696d9") },
                    new Event { Id = Guid.Parse("362bc70d-d132-4a59-b991-5b709732df50"), Name = "Broadway In Chicago", Date = DateTime.ParseExact("2017-10-12", "yyyy-MM-dd", CultureInfo.InvariantCulture, DateTimeStyles.None), Banner = "http://res.cloudinary.com/dby4pjuo8/image/upload/w_700,h_400/Re-ticket%20system/Events/Broadway_In_Chicago.jpg", VenueId = Guid.Parse("bc8e003c-a064-44cd-9963-b011c1757600"), Description = "", CategoryId = Guid.Parse("abb22b0e-656a-47af-8d28-3394cdf50d07") },
                    new Event { Id = Guid.Parse("139d2bce-4179-4500-9033-ca1c4d906462"), Name = "Disney's ALADDIN", Date = DateTime.ParseExact("2017-11-23", "yyyy-MM-dd", CultureInfo.InvariantCulture, DateTimeStyles.None), Banner = "http://res.cloudinary.com/dby4pjuo8/image/upload/w_700,h_400/Re-ticket%20system/Events/Disney_s_ALADDIN.jpg", VenueId = Guid.Parse("bc8e003c-a064-44cd-9963-b011c1757600"), Description = "", CategoryId = Guid.Parse("abb22b0e-656a-47af-8d28-3394cdf50d07") },
                    new Event { Id = Guid.Parse("56a2229f-d160-44a9-b20f-bbb42b098b24"), Name = "Yacht Party", Date = DateTime.ParseExact("2017-08-16", "yyyy-MM-dd", CultureInfo.InvariantCulture, DateTimeStyles.None), Banner = "http://res.cloudinary.com/dby4pjuo8/image/upload/w_700,h_400/Re-ticket%20system/Events/Yacht_Party.jpg", VenueId = Guid.Parse("bcab8f9c-fc5c-462c-ad90-58128bcaf954"), Description = "", CategoryId = Guid.Parse("b1461260-149f-43ee-815b-7ee6e9af716f") },
                    new Event { Id = Guid.Parse("2cc586a4-4b5f-4938-bddc-dac5e2f15886"), Name = "Chicago River Sunset", Date = DateTime.ParseExact("2017-08-09", "yyyy-MM-dd", CultureInfo.InvariantCulture, DateTimeStyles.None), Banner = "http://res.cloudinary.com/dby4pjuo8/image/upload/w_700,h_400/Re-ticket%20system/Events/Chicago_River_Sunset.jpg", VenueId = Guid.Parse("bcab8f9c-fc5c-462c-ad90-58128bcaf954"), Description = "", CategoryId = Guid.Parse("fc694ff8-78be-4c2b-83ef-d0cf55020924") },
                    new Event { Id = Guid.Parse("48edca50-f65b-4149-bea5-e32543ce0775"), Name = "Singing for Our Supper", Date = DateTime.ParseExact("2017-09-13", "yyyy-MM-dd", CultureInfo.InvariantCulture, DateTimeStyles.None), Banner = "http://res.cloudinary.com/dby4pjuo8/image/upload/w_700,h_400/Re-ticket%20system/Events/Singing_for_Our_Supper.jpg", VenueId = Guid.Parse("a1596c8b-4e7b-4bed-9f1b-3915f5f1d5ed"), Description = "", CategoryId = Guid.Parse("b19cdda5-c7a3-46f3-b8aa-3599576696d9") },
                    new Event { Id = Guid.Parse("6f02c4c6-6441-4a2e-8884-da9e888bd9a5"), Name = "The Bridges of Madison County", Date = DateTime.ParseExact("2017-09-16", "yyyy-MM-dd", CultureInfo.InvariantCulture, DateTimeStyles.None), Banner = "http://res.cloudinary.com/dby4pjuo8/image/upload/w_700,h_400/Re-ticket%20system/Events/The_Bridges_of_Madison_County.jpg", VenueId = Guid.Parse("a1596c8b-4e7b-4bed-9f1b-3915f5f1d5ed"), Description = "", CategoryId = Guid.Parse("abb22b0e-656a-47af-8d28-3394cdf50d07") },
                    new Event { Id = Guid.Parse("28dd818e-0f06-4555-9759-b32a321aebf6"), Name = "Moonlight Boat Party", Date = DateTime.ParseExact("2017-11-18", "yyyy-MM-dd", CultureInfo.InvariantCulture, DateTimeStyles.None), Banner = "http://res.cloudinary.com/dby4pjuo8/image/upload/w_700,h_400/Re-ticket%20system/Events/Moonlight_Boat_Party.jpg", VenueId = Guid.Parse("a487c2f8-568c-42c9-8d86-4c1e05c06b04"), Description = "", CategoryId = Guid.Parse("b1461260-149f-43ee-815b-7ee6e9af716f") },
                    new Event { Id = Guid.Parse("ef9d91da-ea32-4955-bc53-893f6226297e"), Name = "Harbor Tour - the Tall Ships", Date = DateTime.ParseExact("2017-09-12", "yyyy-MM-dd", CultureInfo.InvariantCulture, DateTimeStyles.None), Banner = "http://res.cloudinary.com/dby4pjuo8/image/upload/w_700,h_400/Re-ticket%20system/Events/Harbor_Tour_to_View_the_Tall_Ships.jpg", VenueId = Guid.Parse("a487c2f8-568c-42c9-8d86-4c1e05c06b04"), Description = "", CategoryId = Guid.Parse("fc694ff8-78be-4c2b-83ef-d0cf55020924") },
                    new Event { Id = Guid.Parse("3cb95ac7-c1ee-4db7-9716-c7aae12b3d6f"), Name = "Mamma Mia! Farewell Tour", Date = DateTime.ParseExact("2017-08-12", "yyyy-MM-dd", CultureInfo.InvariantCulture, DateTimeStyles.None), Banner = "http://res.cloudinary.com/dby4pjuo8/image/upload/w_700,h_400/Re-ticket%20system/Events/Mamma_Mia_Farewell_Tour.jpg", VenueId = Guid.Parse("d5ab42c2-ec4a-4a10-bfb7-ead712d236ca"), Description = "", CategoryId = Guid.Parse("abb22b0e-656a-47af-8d28-3394cdf50d07") },
                    new Event { Id = Guid.Parse("78fcfd0e-4d96-419b-ae3a-cc1dd6ee04c4"), Name = "Beauty and the Beast", Date = DateTime.ParseExact("2017-09-16", "yyyy-MM-dd", CultureInfo.InvariantCulture, DateTimeStyles.None), Banner = "http://res.cloudinary.com/dby4pjuo8/image/upload/w_700,h_400/Re-ticket%20system/Events/Beauty_and_the_Beast.jpg", VenueId = Guid.Parse("d5ab42c2-ec4a-4a10-bfb7-ead712d236ca"), Description = "", CategoryId = Guid.Parse("abb22b0e-656a-47af-8d28-3394cdf50d07") },
                    new Event { Id = Guid.Parse("30084819-0797-4c57-9d85-0dce9b47d277"), Name = "Live Music at City Winery", Date = DateTime.ParseExact("2017-10-17", "yyyy-MM-dd", CultureInfo.InvariantCulture, DateTimeStyles.None), Banner = "http://res.cloudinary.com/dby4pjuo8/image/upload/w_700,h_400/Re-ticket%20system/Events/Live_Music_More_at_City_Winery.jpg", VenueId = Guid.Parse("e40be5c0-6901-4159-8e7f-ae35e7adac98"), Description = "", CategoryId = Guid.Parse("b19cdda5-c7a3-46f3-b8aa-3599576696d9") },
                    new Event { Id = Guid.Parse("fe4e1039-3136-4ebc-ade3-8cc4df3f81c2"), Name = "Magician Peter Morrison", Date = DateTime.ParseExact("2017-09-28", "yyyy-MM-dd", CultureInfo.InvariantCulture, DateTimeStyles.None), Banner = "http://res.cloudinary.com/dby4pjuo8/image/upload/w_700,h_400/Re-ticket%20system/Events/Magician_Peter_Morrison.jpg", VenueId = Guid.Parse("e40be5c0-6901-4159-8e7f-ae35e7adac98"), Description = "", CategoryId = Guid.Parse("abb22b0e-656a-47af-8d28-3394cdf50d07") },
                    new Event { Id = Guid.Parse("9c97f504-308f-4355-9272-7485fec1a387"), Name = "SEA LIFE Arizona", Date = DateTime.ParseExact("2017-10-09", "yyyy-MM-dd", CultureInfo.InvariantCulture, DateTimeStyles.None), Banner = "http://res.cloudinary.com/dby4pjuo8/image/upload/w_700,h_400/Re-ticket%20system/Events/SEA_LIFE_Arizona.jpg", VenueId = Guid.Parse("33a306ab-6e96-45aa-8fb3-753a006c090c"), Description = "", CategoryId = Guid.Parse("7fd85510-368c-45d1-b03d-0daf4083e530") },
                    new Event { Id = Guid.Parse("c1fa8b30-d2f2-4987-960c-6f66dbfcc0d6"), Name = "Wet 'n' Wild Phoenix", Date = DateTime.ParseExact("2017-11-10", "yyyy-MM-dd", CultureInfo.InvariantCulture, DateTimeStyles.None), Banner = "http://res.cloudinary.com/dby4pjuo8/image/upload/w_700,h_400/Re-ticket%20system/Events/Wet_n_Wild_Phoenix.jpg", VenueId = Guid.Parse("33a306ab-6e96-45aa-8fb3-753a006c090c"), Description = "", CategoryId = Guid.Parse("7fd85510-368c-45d1-b03d-0daf4083e530") },
                    new Event { Id = Guid.Parse("253e27b6-e37b-47fc-9266-19e7cd0367fc"), Name = "Beehive: The '60s Musical", Date = DateTime.ParseExact("2017-09-24", "yyyy-MM-dd", CultureInfo.InvariantCulture, DateTimeStyles.None), Banner = "http://res.cloudinary.com/dby4pjuo8/image/upload/w_700,h_400/Re-ticket%20system/Events/Beehive_The_60s_Musical.jpg", VenueId = Guid.Parse("9877a0d6-2f7a-4d13-8a5a-fe659102521a"), Description = "", CategoryId = Guid.Parse("abb22b0e-656a-47af-8d28-3394cdf50d07") },
                    new Event { Id = Guid.Parse("430f5d4b-8603-40c7-afa2-5c658c8c38f1"), Name = "Idina Menzel", Date = DateTime.ParseExact("2017-11-11", "yyyy-MM-dd", CultureInfo.InvariantCulture, DateTimeStyles.None), Banner = "http://res.cloudinary.com/dby4pjuo8/image/upload/w_700,h_400/Re-ticket%20system/Events/Idina_Menzel.jpg", VenueId = Guid.Parse("9877a0d6-2f7a-4d13-8a5a-fe659102521a"), Description = "", CategoryId = Guid.Parse("abb22b0e-656a-47af-8d28-3394cdf50d07") }
            };
            foreach (var ev in _events)
            {
                _unitOfWOrk.Events.Create(ev);
            }

            var _tickets = new Ticket[]
            {
                    new Ticket { Id = Guid.Parse("9562622b-9622-442d-9377-ee27bb0659c4"), EventId = Guid.Parse("430f5d4b-8603-40c7-afa2-5c658c8c38f1"), Price = 16, SellerId = Guid.Parse("73196af2-90f8-479a-8b33-36411128b71e"), TicketType = TicketType.Standart, StatusId = Guid.Parse("97398191-cc77-4d97-b747-71094d0fdff0") },
                    new Ticket { Id = Guid.Parse("23cd43c3-bbf2-4070-aa44-59c47f65b33f"), EventId = Guid.Parse("430f5d4b-8603-40c7-afa2-5c658c8c38f1"), Price = 32, SellerId = Guid.Parse("73196af2-90f8-479a-8b33-36411128b71e"), TicketType = TicketType.Standart, StatusId = Guid.Parse("d4e8d141-70be-4216-9967-f8a6a31cfcc2") },
                    new Ticket { Id = Guid.Parse("fc0bc35b-92ff-498b-958e-574b04af1e6c"), EventId = Guid.Parse("253e27b6-e37b-47fc-9266-19e7cd0367fc"), Price = 11, SellerId = Guid.Parse("73196af2-90f8-479a-8b33-36411128b71e"), TicketType = TicketType.Standart, StatusId = Guid.Parse("83a182ef-be1c-4e8b-ac29-c79e23b378e5") },
                    new Ticket { Id = Guid.Parse("8b1db596-76f5-406d-9a0e-ea26281c4f99"), EventId = Guid.Parse("c1fa8b30-d2f2-4987-960c-6f66dbfcc0d6"), Price = 45, SellerId = Guid.Parse("73196af2-90f8-479a-8b33-36411128b71e"), TicketType = TicketType.Standart, StatusId = Guid.Parse("83a182ef-be1c-4e8b-ac29-c79e23b378e5") },
                    new Ticket { Id = Guid.Parse("b6c5a4b9-cc39-407b-b2b4-284bfafca69f"), EventId = Guid.Parse("c1fa8b30-d2f2-4987-960c-6f66dbfcc0d6"), Price = 51, SellerId = Guid.Parse("73196af2-90f8-479a-8b33-36411128b71e"), TicketType = TicketType.Standart, StatusId = Guid.Parse("83a182ef-be1c-4e8b-ac29-c79e23b378e5") },
                    new Ticket { Id = Guid.Parse("aa73cefa-1f55-47d4-b187-2fa32976fe67"), EventId = Guid.Parse("9c97f504-308f-4355-9272-7485fec1a387"), Price = 14, SellerId = Guid.Parse("07107e6a-b384-4778-ae37-617616537c7c"), TicketType = TicketType.Popular, StatusId = Guid.Parse("97398191-cc77-4d97-b747-71094d0fdff0") },
                    new Ticket { Id = Guid.Parse("6e413954-44e1-480b-a9d3-c34fcd2ddafe"), EventId = Guid.Parse("fe4e1039-3136-4ebc-ade3-8cc4df3f81c2"), Price = 19, SellerId = Guid.Parse("07107e6a-b384-4778-ae37-617616537c7c"), TicketType = TicketType.Standart, StatusId = Guid.Parse("d4e8d141-70be-4216-9967-f8a6a31cfcc2") },
                    new Ticket { Id = Guid.Parse("c3750dd3-43f4-4329-a609-7b15a018d9e7"), EventId = Guid.Parse("fe4e1039-3136-4ebc-ade3-8cc4df3f81c2"), Price = 20, SellerId = Guid.Parse("07107e6a-b384-4778-ae37-617616537c7c"), TicketType = TicketType.Standart, StatusId = Guid.Parse("83a182ef-be1c-4e8b-ac29-c79e23b378e5") },
                    new Ticket { Id = Guid.Parse("2814e0c2-b2a9-42c7-a984-23448f9bc19e"), EventId = Guid.Parse("30084819-0797-4c57-9d85-0dce9b47d277"), Price = 77, SellerId = Guid.Parse("07107e6a-b384-4778-ae37-617616537c7c"), TicketType = TicketType.Standart, StatusId = Guid.Parse("83a182ef-be1c-4e8b-ac29-c79e23b378e5") },
                    new Ticket { Id = Guid.Parse("eb7fbd88-8e0c-4e0b-ad39-f5f521a8f845"), EventId = Guid.Parse("30084819-0797-4c57-9d85-0dce9b47d277"), Price = 132, SellerId = Guid.Parse("07107e6a-b384-4778-ae37-617616537c7c"), TicketType = TicketType.Important, StatusId = Guid.Parse("97398191-cc77-4d97-b747-71094d0fdff0") },
                    new Ticket { Id = Guid.Parse("31344de9-9e74-4431-998c-60438ce07cac"), EventId = Guid.Parse("78fcfd0e-4d96-419b-ae3a-cc1dd6ee04c4"), Price = 19, SellerId = Guid.Parse("07107e6a-b384-4778-ae37-617616537c7c"), TicketType = TicketType.Standart, StatusId = Guid.Parse("83a182ef-be1c-4e8b-ac29-c79e23b378e5") },
                    new Ticket { Id = Guid.Parse("bc2acfdd-a05e-4a14-820b-5fd5b7913715"), EventId = Guid.Parse("3cb95ac7-c1ee-4db7-9716-c7aae12b3d6f"), Price = 59, SellerId = Guid.Parse("071619af-4cf9-4531-bd77-2502736b4000"), TicketType = TicketType.Standart, StatusId = Guid.Parse("97398191-cc77-4d97-b747-71094d0fdff0") },
                    new Ticket { Id = Guid.Parse("82fec890-db2f-421f-b576-4414ef5417fc"), EventId = Guid.Parse("3cb95ac7-c1ee-4db7-9716-c7aae12b3d6f"), Price = 34, SellerId = Guid.Parse("071619af-4cf9-4531-bd77-2502736b4000"), TicketType = TicketType.Standart, StatusId = Guid.Parse("83a182ef-be1c-4e8b-ac29-c79e23b378e5") },
                    new Ticket { Id = Guid.Parse("61b35b7a-465d-4bba-aefb-0839d2f7f102"), EventId = Guid.Parse("ef9d91da-ea32-4955-bc53-893f6226297e"), Price = 25, SellerId = Guid.Parse("071619af-4cf9-4531-bd77-2502736b4000"), TicketType = TicketType.Popular, StatusId = Guid.Parse("83a182ef-be1c-4e8b-ac29-c79e23b378e5") },
                    new Ticket { Id = Guid.Parse("0ed99b40-fea0-45ca-bf02-81c79c90e2f2"), EventId = Guid.Parse("28dd818e-0f06-4555-9759-b32a321aebf6"), Price = 34, SellerId = Guid.Parse("071619af-4cf9-4531-bd77-2502736b4000"), TicketType = TicketType.Standart, StatusId = Guid.Parse("83a182ef-be1c-4e8b-ac29-c79e23b378e5") },
                    new Ticket { Id = Guid.Parse("e7bcf98c-3f24-4ebe-83e3-0add2aa8adb5"), EventId = Guid.Parse("28dd818e-0f06-4555-9759-b32a321aebf6"), Price = 12, SellerId = Guid.Parse("071619af-4cf9-4531-bd77-2502736b4000"), TicketType = TicketType.Standart, StatusId = Guid.Parse("97398191-cc77-4d97-b747-71094d0fdff0") },
                    new Ticket { Id = Guid.Parse("6331e367-9dc3-4318-ba4c-4431458bc82b"), EventId = Guid.Parse("28dd818e-0f06-4555-9759-b32a321aebf6"), Price = 19, SellerId = Guid.Parse("071619af-4cf9-4531-bd77-2502736b4000"), TicketType = TicketType.Popular, StatusId = Guid.Parse("97398191-cc77-4d97-b747-71094d0fdff0") },
                    new Ticket { Id = Guid.Parse("722288fd-f29e-4143-bd01-085b4614fd76"), EventId = Guid.Parse("6f02c4c6-6441-4a2e-8884-da9e888bd9a5"), Price = 29, SellerId = Guid.Parse("73196af2-90f8-479a-8b33-36411128b71e"), TicketType = TicketType.Standart, StatusId = Guid.Parse("83a182ef-be1c-4e8b-ac29-c79e23b378e5") },
                    new Ticket { Id = Guid.Parse("6e20211c-8898-4ba2-bbb7-cf1acb069173"), EventId = Guid.Parse("48edca50-f65b-4149-bea5-e32543ce0775"), Price = 56, SellerId = Guid.Parse("73196af2-90f8-479a-8b33-36411128b71e"), TicketType = TicketType.Standart, StatusId = Guid.Parse("83a182ef-be1c-4e8b-ac29-c79e23b378e5") },
                    new Ticket { Id = Guid.Parse("4f4364b4-cb5b-4fef-9ab4-a610136ac816"), EventId = Guid.Parse("2cc586a4-4b5f-4938-bddc-dac5e2f15886"), Price = 48, SellerId = Guid.Parse("73196af2-90f8-479a-8b33-36411128b71e"), TicketType = TicketType.Popular, StatusId = Guid.Parse("83a182ef-be1c-4e8b-ac29-c79e23b378e5") },
                    new Ticket { Id = Guid.Parse("8e9a37b3-2227-4346-888d-20b278c3723a"), EventId = Guid.Parse("56a2229f-d160-44a9-b20f-bbb42b098b24"), Price = 43, SellerId = Guid.Parse("73196af2-90f8-479a-8b33-36411128b71e"), TicketType = TicketType.Standart, StatusId = Guid.Parse("83a182ef-be1c-4e8b-ac29-c79e23b378e5") },
                    new Ticket { Id = Guid.Parse("ac5c03b5-804a-46af-abc5-6c6079cc10c8"), EventId = Guid.Parse("139d2bce-4179-4500-9033-ca1c4d906462"), Price = 19, SellerId = Guid.Parse("73196af2-90f8-479a-8b33-36411128b71e"), TicketType = TicketType.Important, StatusId = Guid.Parse("83a182ef-be1c-4e8b-ac29-c79e23b378e5") },
                    new Ticket { Id = Guid.Parse("421fe85a-396c-4f51-8640-ea3ec973e66a"), EventId = Guid.Parse("362bc70d-d132-4a59-b991-5b709732df50"), Price = 27, SellerId = Guid.Parse("071619af-4cf9-4531-bd77-2502736b4000"), TicketType = TicketType.Important, StatusId = Guid.Parse("83a182ef-be1c-4e8b-ac29-c79e23b378e5") },
                    new Ticket { Id = Guid.Parse("1c837ea9-1fd6-4acf-88c9-8d31367a091a"), EventId = Guid.Parse("106384b8-4aa0-4652-b3e4-30a5c9f17350"), Price = 29, SellerId = Guid.Parse("071619af-4cf9-4531-bd77-2502736b4000"), TicketType = TicketType.Popular, StatusId = Guid.Parse("83a182ef-be1c-4e8b-ac29-c79e23b378e5") },
                    new Ticket { Id = Guid.Parse("cef5df4c-895e-4bca-98a2-904e66b17a64"), EventId = Guid.Parse("3a363d90-e7e2-4282-b510-491329851fc6"), Price = 28, SellerId = Guid.Parse("071619af-4cf9-4531-bd77-2502736b4000"), TicketType = TicketType.Standart, StatusId = Guid.Parse("83a182ef-be1c-4e8b-ac29-c79e23b378e5") },
                    new Ticket { Id = Guid.Parse("ffdd0aee-f658-45a5-8b09-e48f94031836"), EventId = Guid.Parse("650148f5-9802-4000-81c3-cad1ef5576e6"), Price = 37, SellerId = Guid.Parse("071619af-4cf9-4531-bd77-2502736b4000"), TicketType = TicketType.Popular, StatusId = Guid.Parse("83a182ef-be1c-4e8b-ac29-c79e23b378e5") },
                    new Ticket { Id = Guid.Parse("a73c7397-6c4d-4ef3-9cea-2f512259b59e"), EventId = Guid.Parse("1c90a9d5-6712-4d8f-9f55-619f5a6f2093"), Price = 48, SellerId = Guid.Parse("071619af-4cf9-4531-bd77-2502736b4000"), TicketType = TicketType.Standart, StatusId = Guid.Parse("83a182ef-be1c-4e8b-ac29-c79e23b378e5") },
                    new Ticket { Id = Guid.Parse("97596aae-be85-4c45-97a5-1320646f4501"), EventId = Guid.Parse("131ae463-8303-46e1-8c5c-82ab4c948083"), Price = 34, SellerId = Guid.Parse("071619af-4cf9-4531-bd77-2502736b4000"), TicketType = TicketType.Standart, StatusId = Guid.Parse("83a182ef-be1c-4e8b-ac29-c79e23b378e5") },
                    new Ticket { Id = Guid.Parse("fa8fc65b-e541-4d41-b2a0-d5508d5f91a1"), EventId = Guid.Parse("d284d394-9af7-42fc-ab16-de998ac2bac8"), Price = 46, SellerId = Guid.Parse("07107e6a-b384-4778-ae37-617616537c7c"), TicketType = TicketType.Standart, StatusId = Guid.Parse("83a182ef-be1c-4e8b-ac29-c79e23b378e5") },
                    new Ticket { Id = Guid.Parse("2b9b1975-7c09-4006-956e-4f00e7fc0fd6"), EventId = Guid.Parse("8df34096-ac54-4936-91a0-646983840c57"), Price = 45, SellerId = Guid.Parse("07107e6a-b384-4778-ae37-617616537c7c"), TicketType = TicketType.Popular, StatusId = Guid.Parse("83a182ef-be1c-4e8b-ac29-c79e23b378e5") },
                    new Ticket { Id = Guid.Parse("6ed96d93-6dfc-46e7-bbb8-4af81498511c"), EventId = Guid.Parse("7f38e8bd-d211-49db-955d-c5d9a04d6c3d"), Price = 31, SellerId = Guid.Parse("07107e6a-b384-4778-ae37-617616537c7c"), TicketType = TicketType.Standart, StatusId = Guid.Parse("83a182ef-be1c-4e8b-ac29-c79e23b378e5") },
                    new Ticket { Id = Guid.Parse("f77afac6-e68e-4c2a-b6c6-a6425e3604e0"), EventId = Guid.Parse("7f38e8bd-d211-49db-955d-c5d9a04d6c3d"), Price = 26, SellerId = Guid.Parse("07107e6a-b384-4778-ae37-617616537c7c"), TicketType = TicketType.Standart, StatusId = Guid.Parse("83a182ef-be1c-4e8b-ac29-c79e23b378e5") },
                    new Ticket { Id = Guid.Parse("4534a799-cc36-490e-9e93-6e37543dd37b"), EventId = Guid.Parse("d284d394-9af7-42fc-ab16-de998ac2bac8"), Price = 37, SellerId = Guid.Parse("07107e6a-b384-4778-ae37-617616537c7c"), TicketType = TicketType.Standart, StatusId = Guid.Parse("83a182ef-be1c-4e8b-ac29-c79e23b378e5") },
                    new Ticket { Id = Guid.Parse("76e1e61f-f8c3-4aa1-a952-ef65c6d893d6"), EventId = Guid.Parse("650148f5-9802-4000-81c3-cad1ef5576e6"), Price = 41, SellerId = Guid.Parse("07107e6a-b384-4778-ae37-617616537c7c"), TicketType = TicketType.Important, StatusId = Guid.Parse("83a182ef-be1c-4e8b-ac29-c79e23b378e5") },
                    new Ticket { Id = Guid.Parse("d2771ffe-69eb-4489-80df-6e0bad2ff16f"), EventId = Guid.Parse("650148f5-9802-4000-81c3-cad1ef5576e6"), Price = 42, SellerId = Guid.Parse("73196af2-90f8-479a-8b33-36411128b71e"), TicketType = TicketType.Standart, StatusId = Guid.Parse("d4e8d141-70be-4216-9967-f8a6a31cfcc2") },
                    new Ticket { Id = Guid.Parse("98623a1a-b5d6-492b-9d70-9ff3722fecc1"), EventId = Guid.Parse("106384b8-4aa0-4652-b3e4-30a5c9f17350"), Price = 22, SellerId = Guid.Parse("73196af2-90f8-479a-8b33-36411128b71e"), TicketType = TicketType.Popular, StatusId = Guid.Parse("83a182ef-be1c-4e8b-ac29-c79e23b378e5") },
                    new Ticket { Id = Guid.Parse("ba7d88f6-9b05-4785-9f6c-211120642a17"), EventId = Guid.Parse("362bc70d-d132-4a59-b991-5b709732df50"), Price = 23, SellerId = Guid.Parse("73196af2-90f8-479a-8b33-36411128b71e"), TicketType = TicketType.Standart, StatusId = Guid.Parse("83a182ef-be1c-4e8b-ac29-c79e23b378e5") },
                    new Ticket { Id = Guid.Parse("b71aabb9-d454-48e4-95dc-7d9945ac560a"), EventId = Guid.Parse("139d2bce-4179-4500-9033-ca1c4d906462"), Price = 55, SellerId = Guid.Parse("73196af2-90f8-479a-8b33-36411128b71e"), TicketType = TicketType.Popular, StatusId = Guid.Parse("83a182ef-be1c-4e8b-ac29-c79e23b378e5") },
                    new Ticket { Id = Guid.Parse("ecc00676-f8f4-42a8-85a2-d3da5d25c81c"), EventId = Guid.Parse("48edca50-f65b-4149-bea5-e32543ce0775"), Price = 52, SellerId = Guid.Parse("73196af2-90f8-479a-8b33-36411128b71e"), TicketType = TicketType.Standart, StatusId = Guid.Parse("83a182ef-be1c-4e8b-ac29-c79e23b378e5") },
                    new Ticket { Id = Guid.Parse("9cee7a39-0876-478f-b9fe-2ba34827cd73"), EventId = Guid.Parse("56a2229f-d160-44a9-b20f-bbb42b098b24"), Price = 40, SellerId = Guid.Parse("071619af-4cf9-4531-bd77-2502736b4000"), TicketType = TicketType.Standart, StatusId = Guid.Parse("83a182ef-be1c-4e8b-ac29-c79e23b378e5") },
                    new Ticket { Id = Guid.Parse("2d5ced3b-6122-444a-bdd4-ee39425d69e8"), EventId = Guid.Parse("131ae463-8303-46e1-8c5c-82ab4c948083"), Price = 20, SellerId = Guid.Parse("071619af-4cf9-4531-bd77-2502736b4000"), TicketType = TicketType.Popular, StatusId = Guid.Parse("83a182ef-be1c-4e8b-ac29-c79e23b378e5") }
            };
            foreach (var ticket in _tickets)
            {
                _unitOfWOrk.Tickets.Create(ticket);
            }

            var _orders = new Order[]
            {
                    new Order { Id = Guid.Parse("74ea52de-82e1-48aa-8e21-5fa0bac1c6de"), TicketId = Guid.Parse("23cd43c3-bbf2-4070-aa44-59c47f65b33f"), StatusId = Guid.Parse("680d1cad-881a-4791-b684-4749bcfe8879"), BuyerId = Guid.Parse("07107e6a-b384-4778-ae37-617616537c7c"),TrackNO = "#2017-11-06-74ea52de-82e1-48aa-8e21-5fa0bac1c6de", Date = DateTime.ParseExact("06/11/2017", "MM/dd/yyyy", CultureInfo.InvariantCulture) },
                    new Order { Id = Guid.Parse("af31692f-1ea8-4d69-987d-55220cc831ce"), TicketId = Guid.Parse("6e413954-44e1-480b-a9d3-c34fcd2ddafe"), StatusId = Guid.Parse("680d1cad-881a-4791-b684-4749bcfe8879"), BuyerId = Guid.Parse("73196af2-90f8-479a-8b33-36411128b71e"),TrackNO = "#2017-03-06-af31692f-1ea8-4d69-987d-55220cc831ce", Date = DateTime.ParseExact("06/03/2017", "MM/dd/yyyy", CultureInfo.InvariantCulture) },
                    new Order { Id = Guid.Parse("f7635950-17ff-4f85-9fc3-8719570c25d1"), TicketId = Guid.Parse("d2771ffe-69eb-4489-80df-6e0bad2ff16f"), StatusId = Guid.Parse("680d1cad-881a-4791-b684-4749bcfe8879"), BuyerId = Guid.Parse("071619af-4cf9-4531-bd77-2502736b4000"),TrackNO = "#2017-07-06-f7635950-17ff-4f85-9fc3-8719570c25d1", Date = DateTime.ParseExact("06/07/2017", "MM/dd/yyyy", CultureInfo.InvariantCulture) },
                    new Order { Id = Guid.Parse("21ac963a-8de1-46f0-bd0e-906186ef5ab7"), TicketId = Guid.Parse("9562622b-9622-442d-9377-ee27bb0659c4"), StatusId = Guid.Parse("97398191-cc77-4d97-b747-71094d0fdff0"), BuyerId = Guid.Parse("071619af-4cf9-4531-bd77-2502736b4000"),TrackNO = "#2017-21-06-21ac963a-8de1-46f0-bd0e-906186ef5ab7", Date = DateTime.ParseExact("06/21/2017", "MM/dd/yyyy", CultureInfo.InvariantCulture) },
                    new Order { Id = Guid.Parse("f0b2009f-8515-4400-b581-07b6aec4f104"), TicketId = Guid.Parse("aa73cefa-1f55-47d4-b187-2fa32976fe67"), StatusId = Guid.Parse("97398191-cc77-4d97-b747-71094d0fdff0"), BuyerId = Guid.Parse("73196af2-90f8-479a-8b33-36411128b71e"),TrackNO = "#2017-30-05-f0b2009f-8515-4400-b581-07b6aec4f104", Date = DateTime.ParseExact("05/30/2017", "MM/dd/yyyy", CultureInfo.InvariantCulture) },
                    new Order { Id = Guid.Parse("18a2ffa9-09ab-4061-9c57-888503a8e465"), TicketId = Guid.Parse("eb7fbd88-8e0c-4e0b-ad39-f5f521a8f845"), StatusId = Guid.Parse("97398191-cc77-4d97-b747-71094d0fdff0"), BuyerId = Guid.Parse("071619af-4cf9-4531-bd77-2502736b4000"),TrackNO = "#2017-05-06-18a2ffa9-09ab-4061-9c57-888503a8e465", Date = DateTime.ParseExact("06/05/2017", "MM/dd/yyyy", CultureInfo.InvariantCulture) },
                    new Order { Id = Guid.Parse("2bc5e17d-0b82-4d98-a5bd-23c37abd79a4"), TicketId = Guid.Parse("bc2acfdd-a05e-4a14-820b-5fd5b7913715"), StatusId = Guid.Parse("97398191-cc77-4d97-b747-71094d0fdff0"), BuyerId = Guid.Parse("07107e6a-b384-4778-ae37-617616537c7c"),TrackNO = "#2017-10-06-2bc5e17d-0b82-4d98-a5bd-23c37abd79a4", Date = DateTime.ParseExact("06/10/2017", "MM/dd/yyyy", CultureInfo.InvariantCulture) },
                    new Order { Id = Guid.Parse("d609c21b-abc0-4a39-b061-bdc66b3eeace"), TicketId = Guid.Parse("e7bcf98c-3f24-4ebe-83e3-0add2aa8adb5"), StatusId = Guid.Parse("97398191-cc77-4d97-b747-71094d0fdff0"), BuyerId = Guid.Parse("73196af2-90f8-479a-8b33-36411128b71e"),TrackNO = "#2017-08-06-d609c21b-abc0-4a39-b061-bdc66b3eeace", Date = DateTime.ParseExact("06/08/2017", "MM/dd/yyyy", CultureInfo.InvariantCulture) },
                    new Order { Id = Guid.Parse("3e4ab9ca-6e48-477a-845d-0b993371a437"), TicketId = Guid.Parse("6331e367-9dc3-4318-ba4c-4431458bc82b"), StatusId = Guid.Parse("97398191-cc77-4d97-b747-71094d0fdff0"), BuyerId = Guid.Parse("07107e6a-b384-4778-ae37-617616537c7c"),TrackNO = "#2017-06-06-3e4ab9ca-6e48-477a-845d-0b993371a437", Date = DateTime.ParseExact("06/06/2017", "MM/dd/yyyy", CultureInfo.InvariantCulture) },
                    new Order { Id = Guid.Parse("a51dde09-da14-4e52-96fa-92dd52c2a074"), TicketId = Guid.Parse("9562622b-9622-442d-9377-ee27bb0659c4"), StatusId = Guid.Parse("97398191-cc77-4d97-b747-71094d0fdff0"), BuyerId = Guid.Parse("07107e6a-b384-4778-ae37-617616537c7c"),TrackNO = "#2017-23-06-a51dde09-da14-4e52-96fa-92dd52c2a074", Date = DateTime.ParseExact("06/23/2017", "MM/dd/yyyy", CultureInfo.InvariantCulture) }
            };
            foreach (var order in _orders)
            {
                _unitOfWOrk.Orders.Create(order);
            }

        }

    }
}
