﻿using Epam.Labs.TrainingProject.AppCore.Entities;
using Epam.Labs.TrainingProject.AppCore.Interfaces;
using Epam.Labs.TrainingProject.Infrastructure.Interfaces;
using System;

namespace Epam.Labs.TrainingProject.Infrastructure.Facades
{
    public class DbDeleteFacade : IDbDeleteFacade
    {
        IUnitOfWork _unitOfWork;

        public DbDeleteFacade(IUnitOfWork unitOfWork)
        {
            _unitOfWork = unitOfWork;
        }

        public void RemoveOrderFromCart(Guid orderId)
        {
            var order = _unitOfWork.Orders.Get(orderId);
            _unitOfWork.Orders.Remove(order);
        }

        public void RemoveCity(City city)
        {
            _unitOfWork.Cities.Remove(city);
        }

        public void RemoveVenue(Venue venue)
        {
            _unitOfWork.Venues.Remove(venue);
        }

        public void RemoveEvent(Event _event)
        {
            _unitOfWork.Events.Remove(_event);
        }
    }
}
