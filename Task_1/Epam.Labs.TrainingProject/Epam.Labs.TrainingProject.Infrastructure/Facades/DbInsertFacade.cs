﻿using Epam.Labs.TrainingProject.Infrastructure.Interfaces;
using System;
using Epam.Labs.TrainingProject.AppCore.Entities;
using Epam.Labs.TrainingProject.AppCore.Interfaces;

namespace Epam.Labs.TrainingProject.Infrastructure.Facades
{
    public class DbInsertFacade : IDbInsertFacade
    {
        IUnitOfWork _unitOfWork;

        public DbInsertFacade(IUnitOfWork unitOfWork)
        {
            _unitOfWork = unitOfWork;
        }

        public void AddCity(City city)
        {
            _unitOfWork.Cities.Create(city);
        }

        public void AddEvent(Event newEvent)
        {
            _unitOfWork.Events.Create(newEvent);
        }

        public void AddNewOrderToCart(Guid ticketId, Guid buyerId, Guid statusId)
        {
            var id = Guid.NewGuid();

            var order = new Order()
            {
                Id = id,
                Date = DateTime.Now,
                TrackNO = "#" + DateTime.Now.ToString("yyyy-dd-MM") + "-" + id.ToString(),
                TicketId = ticketId,
                BuyerId = buyerId,
                StatusId = statusId
            };

            _unitOfWork.Orders.Create(order);
        }

        public void AddNewTicket(Ticket ticket)
        {
            _unitOfWork.Tickets.Create(ticket);
        }

        public void AddNewUser(User user, AuthenticationData authData)
        {
            _unitOfWork.Users.Create(user);
            _unitOfWork.AuthenticationDatas.Create(authData);
        }

        public void AddVenue(Venue venue)
        {
            _unitOfWork.Venues.Create(venue);
        }
    }
}
