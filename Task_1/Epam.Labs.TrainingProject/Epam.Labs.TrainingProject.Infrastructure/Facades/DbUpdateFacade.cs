﻿using Epam.Labs.TrainingProject.AppCore.Entities;
using Epam.Labs.TrainingProject.AppCore.Interfaces;
using Epam.Labs.TrainingProject.Infrastructure.DTO;
using Epam.Labs.TrainingProject.Infrastructure.Interfaces;
using System;
using System.Linq;

namespace Epam.Labs.TrainingProject.Infrastructure.Facades
{
    public class DbUpdateFacade : IDbUpdateFacade
    {
        IUnitOfWork _unitOfWork;

        public DbUpdateFacade(IUnitOfWork unitOfWork)
        {
            _unitOfWork = unitOfWork;
        }

        public void ConfirmOrder(Order order, Guid statusId)
        {
            var _order = _unitOfWork.Orders.Get(order.Id);

            _order.StatusId = statusId;
            _unitOfWork.Orders.Update(_order);
        }

        public void ConfirmOrderTicketChanges(Guid ticketId, Guid ticketStatusId)
        {
            var ticket = _unitOfWork.Tickets.Get(ticketId);

            ticket.StatusId = ticketStatusId;
            _unitOfWork.Tickets.Update(ticket);
        }

        public void EditUserInfo(EditUserInfoDTO model, AuthenticationData auth, User user)
        {
            var _user = _unitOfWork.Users.Get(user.Id);

            _user.FirstName = model.FirstName;
            _user.LastName = model.LastName;
            _user.Address = model.Address;
            _user.Phone = model.Phone;
            _user.CultureId = model.CultureId;

            _unitOfWork.Users.Update(_user);

            var _auth = _unitOfWork.AuthenticationDatas.Get(auth.Id);

            _auth.Email = model.Email;

            _unitOfWork.AuthenticationDatas.Update(_auth);
        }

        public void RejectTicketOrdering(Guid ticketId, Guid orderId, Guid ticketNewStatusId, Guid orderNewStatus, string comment)
        {
            // set order status to "rejected" and add comment
            var _order = _unitOfWork.Orders.Get(orderId);
            _order.StatusId = orderNewStatus;
            _order.RejectionComment = comment;
            _unitOfWork.Orders.Update(_order);

            // if there are no more orders in status "WFC" for ticket - set ticket status to "selling"
            var otherOrdersAmount = _unitOfWork.Orders.GetAll().Where(o => o.TicketId == ticketId && o.Id != orderId && o.StatusId == Guid.Parse("97398191-cc77-4d97-b747-71094d0fdff0")).Count();

            if (otherOrdersAmount == 0)
            {
                // set ticket status to "sold"
                var _ticket = _unitOfWork.Tickets.Get(ticketId);
                _ticket.StatusId = ticketNewStatusId;
                _unitOfWork.Tickets.Update(_ticket);
            }
        }

        public void ConfirmTicketOrdering(Guid ticketId, Guid orderId, Guid ticketNewStatusId, Guid orderNewStatusId, Guid otherOrdersStatus)
        {
            // set ticket status to "sold"
            var _ticket = _unitOfWork.Tickets.Get(ticketId);
            _ticket.StatusId = ticketNewStatusId;
            _unitOfWork.Tickets.Update(_ticket);

            // set order status to "confirmed"
            var _order = _unitOfWork.Orders.Get(orderId);
            _order.StatusId = orderNewStatusId;
            _unitOfWork.Orders.Update(_order);

            // set other orders (of this ticket) status to "rejected"
            var otherOrders = _unitOfWork.Orders.GetAll().Where(o => o.TicketId == ticketId && o.Id != orderId && o.StatusId == Guid.Parse("97398191-cc77-4d97-b747-71094d0fdff0")).ToList();

            foreach (var order in otherOrders)
            {
                RejectTicketOrdering(ticketId, order.Id, ticketNewStatusId, otherOrdersStatus, "Ticket has already been sold.");
            }
        }

        public void GrantRevokeAdminRights(AuthenticationData auth, Guid roleId)
        {
            auth.RoleId = roleId;
            _unitOfWork.AuthenticationDatas.Update(auth);
        }

        public void EditCity(City city, string newName)
        {
            city.Name = newName;
            _unitOfWork.Cities.Update(city);
        }

        public void EditVenue(Venue venue, string newName, Guid cityId)
        {
            venue.Name = newName;
            venue.CityId = cityId;
            _unitOfWork.Venues.Update(venue);
        }

        public void EditEvent(Event _event, string name, DateTime date, Guid venueId, Guid categoryId, string description = null)
        {
            _event.Name = name;
            _event.Date = date;
            _event.VenueId = venueId;
            _event.CategoryId = categoryId;
            _event.Description = description;

            _unitOfWork.Events.Update(_event);
        }
    }
}