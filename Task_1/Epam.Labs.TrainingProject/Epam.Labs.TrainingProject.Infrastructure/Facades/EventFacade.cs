﻿using Epam.Labs.TrainingProject.Infrastructure.Interfaces;
using System.Collections.Generic;
using Epam.Labs.TrainingProject.Infrastructure.DTO;
using Epam.Labs.TrainingProject.AppCore.Interfaces;
using System.Linq;
using Epam.Labs.TrainingProject.AppCore.Entities;
using System;

namespace Epam.Labs.TrainingProject.Infrastructure.Facades
{
    public class EventFacade : IEventFacade
    {
        IUnitOfWork _unitOfWork;

        public EventFacade(IUnitOfWork unitOfWork)
        {
            _unitOfWork = unitOfWork;
        }

        /// <summary>
        /// Get the list of all existing events (with venue and city).
        /// </summary>
        /// <returns></returns>
        public IEnumerable<Event> GetAllEvents()
        {
            return _unitOfWork.Events.GetWithInclude(e => e.Venue, e => e.Venue.City, e => e.Tickets, e => e.Category).ToList();
        }

        public IEnumerable<Event> GetTopEventsOfCategory(Guid categoryId)
        {
            var _events = _unitOfWork.Events.GetWithInclude(e => e.Venue, e => e.Category, e => e.Venue.City).Where(e => e.CategoryId == categoryId).OrderBy(e => e.Date);

            if (_events.Count() > 6)
            {
                return _events.Take(6);
            }
            else
            {
                return _events;
            }
        }

        /// <summary>
        /// Get all events in selected category from the list of all existing events.
        /// </summary>
        /// <param name="categoryId"></param>
        /// <param name="events"></param>
        /// <returns></returns>
        public IEnumerable<Event> GetAllEventsOfCategory(Guid? categoryId, IEnumerable<Event> events)
        {
            return events.Where(e => e.CategoryId == categoryId);
        }

        public EventDTO GetFormattedEvent(Guid id)
        {
            var _event = _unitOfWork.Events.GetWithInclude(e => e.Venue, e => e.Category, e => e.Venue.City, e => e.Tickets).FirstOrDefault(e => e.Id == id);

            decimal min = 0;
            decimal max = 0;

            if (_event.Tickets.Count > 0)
            {
                min = (from p in _unitOfWork.Tickets.GetAll().Where(t => t.EventId == _event.Id) select p.Price).Min();
                max = (from p in _unitOfWork.Tickets.GetAll().Where(t => t.EventId == _event.Id) select p.Price).Max();
            }

            EventDTO newEvent = new EventDTO
            {
                Id = _event.Id,
                Name = _event.Name,
                Date = _event.Date,
                Banner = _event.Banner,
                Venue = _event.Venue,
                Category = _event.Category,
                Description = _event.Description,
                MinTicketPrice = min,
                MaxTicketPrice = max
            };

            return newEvent;
        }

        public int GetAmountOfRowsWithEvents(int eventsAmount)
        {
            if (eventsAmount % 3 == 0)
            {
                return eventsAmount / 3;
            }
            else
            {
                return eventsAmount / 3 + 1;
            }
        }

        public Event GetEventById(Guid id)
        {
            return _unitOfWork.Events.GetWithInclude(e => e.Venue, e => e.Category, e => e.Venue.City).SingleOrDefault(e => e.Id == id);
        }

        public Event GetEventByName(string name)
        {
            return _unitOfWork.Events.GetAll().FirstOrDefault(e => e.Name == name);
        }

        public string GetEventNameById(Guid? id)
        {
            return _unitOfWork.Events.GetAll().FirstOrDefault(e => e.Id == id).Name;
        }

        /// <summary>
        /// Get 6 most upcoming events.
        /// </summary>
        /// <returns></returns>
        public List<Event> GetUpcomingEvents()
        {
            return _unitOfWork.Events.GetWithInclude(e => e.Venue, ev => ev.Venue.City).OrderBy(e => e.Date).Take(6).ToList();
        }
    }
}
