﻿using Epam.Labs.TrainingProject.Infrastructure.Interfaces;
using System;
using System.Collections.Generic;
using Epam.Labs.TrainingProject.AppCore.Entities;
using Epam.Labs.TrainingProject.AppCore.Interfaces;
using System.Linq;

namespace Epam.Labs.TrainingProject.Infrastructure.Facades
{
    public class OrderFacade : IOrderFacade
    {
        IUnitOfWork _unitOfWork;

        public OrderFacade(IUnitOfWork unitOfWork)
        {
            _unitOfWork = unitOfWork;
        }

        public Order GetOrderById(Guid id)
        {
            return _unitOfWork.Orders.Get(id);
        }

        public IEnumerable<Order> GetOrdersByBuyerAndStatus(Guid buyerId, string status)
        {
            return _unitOfWork.Orders.GetWithInclude(o => o.Buyer, o => o.Status, o => o.Ticket, o => o.Ticket.Seller, o => o.Ticket.Event).Where(o => o.Buyer.Id == buyerId && o.Status.Name == status).ToList();
        }

        public Order GetOrderByTicketId(Guid id)
        {
            return _unitOfWork.Orders.GetAll().FirstOrDefault(o => o.TicketId == id);
        }

        public IEnumerable<Order> GetAllOrdersOfTicketById(Guid id, Guid statusId)
        {
            return _unitOfWork.Orders.GetWithInclude(o => o.Buyer, o => o.Ticket, o => o.Ticket.Event).Where(o => o.TicketId == id && o.StatusId == statusId).ToList();
        }
    }
}
