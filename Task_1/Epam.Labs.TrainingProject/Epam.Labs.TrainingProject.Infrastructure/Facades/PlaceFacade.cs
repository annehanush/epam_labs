﻿using Epam.Labs.TrainingProject.Infrastructure.Interfaces;
using System.Collections.Generic;
using Epam.Labs.TrainingProject.AppCore.Entities;
using Epam.Labs.TrainingProject.AppCore.Interfaces;
using System.Linq;
using System;

namespace Epam.Labs.TrainingProject.Infrastructure.Facades
{
    public class PlaceFacade : IPlaceFacade
    {
        IUnitOfWork _unitOfWork;

        public PlaceFacade(IUnitOfWork unitOfWork)
        {
            _unitOfWork = unitOfWork;
        }

        public IEnumerable<City> GetAllCities()
        {
            return _unitOfWork.Cities.GetWithInclude(c => c.Venues).OrderBy(c => c.Name);
        }

        public City GetCityById(Guid id)
        {
            return _unitOfWork.Cities.Get(id);
        } 

        public City GetCityByName(string name)
        {
            return _unitOfWork.Cities.GetAll().FirstOrDefault(c => c.Name == name);
        }

        public IEnumerable<Venue> GetAllVenues()
        {
            return _unitOfWork.Venues.GetWithInclude(v => v.City, v => v.Events).OrderBy(v => v.City.Name);
        }

        public Venue GetVenueById(Guid id)
        {
            return _unitOfWork.Venues.GetWithInclude(v => v.City).FirstOrDefault(v => v.Id == id);
        }

        public Venue GetVenueByName(string name)
        {
            return _unitOfWork.Venues.GetAll().FirstOrDefault(v => v.Name == name);
        }
    }
}
