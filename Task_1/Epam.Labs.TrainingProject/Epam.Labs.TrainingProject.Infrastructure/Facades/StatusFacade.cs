﻿using Epam.Labs.TrainingProject.Infrastructure.Interfaces;
using Epam.Labs.TrainingProject.AppCore.Entities;
using Epam.Labs.TrainingProject.AppCore.Interfaces;
using System.Linq;
using System;

namespace Epam.Labs.TrainingProject.Infrastructure.Facades
{
    public class StatusFacade : IStatusFacade
    {
        IUnitOfWork _unitOfWork;

        public StatusFacade(IUnitOfWork unitOfWork)
        {
            _unitOfWork = unitOfWork;
        }

        public Status GetStatusByItsName(string name)
        {
            return _unitOfWork.Statuses.GetAll().FirstOrDefault(s => s.Name == name);
        }

        public Guid GetOrderNewStatusAfterConfirmation()
        {
            return _unitOfWork.Statuses.GetAll().FirstOrDefault(s => s.Name == "Confirmed").Id;
        }

        public Guid GetIdOfStatusForOrderConfirmation()
        {
            return _unitOfWork.Statuses.GetAll().FirstOrDefault(s => s.Name == "Waiting for seller Confirmation").Id;
        }

        public Guid GetTicketNewStatusAfterConfirmation()
        {
            return _unitOfWork.Statuses.GetAll().FirstOrDefault(s => s.Name == "Sold").Id;
        }

        public Guid GetIdOfStatusForAddingToCart()
        {
            return _unitOfWork.Statuses.GetAll().FirstOrDefault(s => s.Name == "Waiting for buyer Confirmation").Id;
        }

        public Guid GetIdOfTicketStatusForConfirmOrder()
        {
            return _unitOfWork.Statuses.GetAll().FirstOrDefault(s => s.Name == "Waiting for seller Confirmation").Id;
        }
    }
}
