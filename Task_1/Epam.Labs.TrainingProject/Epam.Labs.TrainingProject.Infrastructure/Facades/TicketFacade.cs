﻿using Epam.Labs.TrainingProject.Infrastructure.Interfaces;
using System;
using System.Collections.Generic;
using Epam.Labs.TrainingProject.AppCore.Entities;
using Epam.Labs.TrainingProject.AppCore.Interfaces;
using System.Linq;

namespace Epam.Labs.TrainingProject.Infrastructure.Facades
{
    public class TicketFacade : ITicketFacade
    {
        IUnitOfWork _unitOfWork;

        public TicketFacade(IUnitOfWork unitOfWork)
        {
            _unitOfWork = unitOfWork;
        }
        
        public IEnumerable<Ticket> GetAllNotSoldImportantTickets()
        {
            return _unitOfWork.Tickets.GetWithInclude(t => t.Event, t => t.Seller, t => t.Status).Where(t => t.TicketType == TicketType.Important && t.Status.Name != "Sold").Take(4).ToList();
        }
        
        public IEnumerable<Ticket> GetAllNotSoldNotImportantTickets()
        {
            return _unitOfWork.Tickets.GetWithInclude(t => t.Event, t => t.Seller, t => t.Status).Where(t => t.TicketType != TicketType.Important && t.Status.Name != "Sold").ToList();
        }
        
        public IEnumerable<Ticket> GetAllTicketsOfEventById(Guid? id, IEnumerable<Ticket> tickets)
        {
            return tickets.Where(t => t.Event.Id == id).OrderBy(t => t.TicketType).ToList();
        }
        
        public int GetAmountOfRowsWithTickets(int ticketsAmount)
        {
            if (ticketsAmount % 4 == 0)
            {
                return ticketsAmount / 4;
            }
            else
            {
                return ticketsAmount / 4 + 1;
            }
        }
        
        public Ticket GetTicketById(Guid id)
        {
            return _unitOfWork.Tickets.GetWithInclude(t => t.Event, t => t.Seller, t => t.Status, t => t.Event.Venue, t => t.Event.Venue.City).FirstOrDefault(t => t.Id == id);
        }

        public IEnumerable<Ticket> GetTicketsBySellerAndStatus(Guid sellerId, string status)
        {
            return _unitOfWork.Tickets.GetWithInclude(t => t.Seller, t => t.Status, t => t.Event).Where(t => t.SellerId == sellerId && t.Status.Name == status);
        }

        public Ticket CreateTicket(Guid eventId, decimal price, Guid sellerId)
        {
            var ticket = new Ticket()
            {
                Id = Guid.NewGuid(),
                Price = price,
                TicketType = TicketType.Standart,
                EventId = eventId,
                StatusId = Guid.Parse("83a182ef-be1c-4e8b-ac29-c79e23b378e5"),
                SellerId = sellerId
            };

            return ticket;
        }
    }
}
