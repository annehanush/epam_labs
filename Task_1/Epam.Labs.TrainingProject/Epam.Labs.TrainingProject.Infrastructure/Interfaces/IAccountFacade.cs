﻿using Epam.Labs.TrainingProject.AppCore.Entities;
using System;
using System.Collections.Generic;

namespace Epam.Labs.TrainingProject.Infrastructure.Interfaces
{
    public interface IAccountFacade
    {
        AuthenticationData GetAuthenticationByUsernameAmdPassword(string username, string password);
        AuthenticationData GetAuthenticationByUsername(string username);
        Role GetRoleByName(string name);
        Role GetRoleById(Guid id);
        int GetUserCartItemsAmount(string username);
        IEnumerable<AuthenticationData> GetAllAppUsers(string currentUser);
    }
}
