﻿using Epam.Labs.TrainingProject.AppCore.Entities;
using Epam.Labs.TrainingProject.Infrastructure.DTO;
using System;
using System.Collections.Generic;

namespace Epam.Labs.TrainingProject.Infrastructure.Interfaces
{
    public interface ICategoryFacade
    {
        IEnumerable<Category> GetAllCategories();
        Category GetCategory(Guid id);
        IEnumerable<Category> GetAllCategoriesWhereEventsExist();
        IEnumerable<CategoryWithTopEventDTO> GetCategoriesWithTopEvents();
    }
}
