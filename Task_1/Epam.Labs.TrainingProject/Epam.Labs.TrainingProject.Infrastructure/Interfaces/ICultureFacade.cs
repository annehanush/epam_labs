﻿using Epam.Labs.TrainingProject.AppCore.Entities;
using System.Collections.Generic;

namespace Epam.Labs.TrainingProject.Infrastructure.Interfaces
{
    public interface ICultureFacade
    {
        Culture GetCultureByName(string name);
        IEnumerable<Culture> GetAllCultures();
        List<string> GetAllCulturesNames();
    }
}
