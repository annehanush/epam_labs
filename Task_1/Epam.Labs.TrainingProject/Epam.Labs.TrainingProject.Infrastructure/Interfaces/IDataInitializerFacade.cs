﻿namespace Epam.Labs.TrainingProject.Infrastructure.Interfaces
{
    public interface IDataInitializerFacade
    {
        void Initialize();
    }
}
