﻿using Epam.Labs.TrainingProject.AppCore.Entities;
using System;

namespace Epam.Labs.TrainingProject.Infrastructure.Interfaces
{
    public interface IDbDeleteFacade
    {
        void RemoveOrderFromCart(Guid orderId);
        void RemoveCity(City city);
        void RemoveVenue(Venue venue);
        void RemoveEvent(Event _event);
    }
}
