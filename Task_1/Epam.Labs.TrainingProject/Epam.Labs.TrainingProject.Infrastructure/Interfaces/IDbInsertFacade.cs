﻿using Epam.Labs.TrainingProject.AppCore.Entities;
using System;

namespace Epam.Labs.TrainingProject.Infrastructure.Interfaces
{
    public interface IDbInsertFacade
    {
        void AddNewUser(User user, AuthenticationData authData);
        void AddNewOrderToCart(Guid ticketId, Guid buyerId, Guid statusId);
        void AddNewTicket(Ticket ticket);
        void AddCity(City city);
        void AddVenue(Venue venue);
        void AddEvent(Event newEvent);
    }
}
