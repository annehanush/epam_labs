﻿using Epam.Labs.TrainingProject.AppCore.Entities;
using Epam.Labs.TrainingProject.Infrastructure.DTO;
using System;

namespace Epam.Labs.TrainingProject.Infrastructure.Interfaces
{
    public interface IDbUpdateFacade
    {
        void ConfirmOrder(Order order, Guid statusId);
        void EditUserInfo(EditUserInfoDTO model, AuthenticationData auth, User user);
        void ConfirmTicketOrdering(Guid ticketId, Guid orderId, Guid ticketNewStatusId, Guid orderNewStatusId, Guid otherOrdersStatus);
        void ConfirmOrderTicketChanges(Guid ticketId, Guid ticketStatusId);
        void RejectTicketOrdering(Guid ticketId, Guid orderId, Guid ticketNewStatusId, Guid orderNewStatus, string comment);
        void GrantRevokeAdminRights(AuthenticationData auth, Guid roleId);
        void EditCity(City city, string newName);
        void EditVenue(Venue venue, string newName, Guid cityId);
        void EditEvent(Event _event, string name, DateTime date, Guid venueId, Guid categoryId, string description = null);
    }
}
