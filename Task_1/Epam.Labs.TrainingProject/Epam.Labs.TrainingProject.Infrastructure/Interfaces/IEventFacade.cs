﻿using Epam.Labs.TrainingProject.AppCore.Entities;
using Epam.Labs.TrainingProject.Infrastructure.DTO;
using System;
using System.Collections.Generic;

namespace Epam.Labs.TrainingProject.Infrastructure.Interfaces
{
    public interface IEventFacade
    {
        IEnumerable<Event> GetAllEvents();
        IEnumerable<Event> GetTopEventsOfCategory(Guid categoryId);
        IEnumerable<Event> GetAllEventsOfCategory(Guid? categoryId, IEnumerable<Event> events);
        EventDTO GetFormattedEvent(Guid id);
        List<Event> GetUpcomingEvents();
        Event GetEventById(Guid id);
        int GetAmountOfRowsWithEvents(int eventsAmount);
        string GetEventNameById(Guid? id);
        Event GetEventByName(string name);
    }
}
