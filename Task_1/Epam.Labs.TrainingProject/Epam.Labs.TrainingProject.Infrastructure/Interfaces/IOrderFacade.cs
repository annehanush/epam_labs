﻿using Epam.Labs.TrainingProject.AppCore.Entities;
using System;
using System.Collections.Generic;

namespace Epam.Labs.TrainingProject.Infrastructure.Interfaces
{
    public interface IOrderFacade
    {
        IEnumerable<Order> GetOrdersByBuyerAndStatus(Guid buyerId, string status);
        Order GetOrderByTicketId(Guid id);
        Order GetOrderById(Guid id);
        IEnumerable<Order> GetAllOrdersOfTicketById(Guid id, Guid statusId);
    }
}
