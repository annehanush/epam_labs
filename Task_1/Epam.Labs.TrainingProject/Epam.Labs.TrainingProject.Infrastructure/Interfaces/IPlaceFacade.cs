﻿using Epam.Labs.TrainingProject.AppCore.Entities;
using System;
using System.Collections.Generic;

namespace Epam.Labs.TrainingProject.Infrastructure.Interfaces
{
    public interface IPlaceFacade
    {
        IEnumerable<City> GetAllCities();
        City GetCityById(Guid id);
        City GetCityByName(string name);
        IEnumerable<Venue> GetAllVenues();
        Venue GetVenueById(Guid id);
        Venue GetVenueByName(string name);
    }
}
