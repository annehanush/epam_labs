﻿using Epam.Labs.TrainingProject.AppCore.Entities;
using System;

namespace Epam.Labs.TrainingProject.Infrastructure.Interfaces
{
    public interface IStatusFacade
    {
        Status GetStatusByItsName(string name);
        Guid GetIdOfStatusForOrderConfirmation();
        Guid GetOrderNewStatusAfterConfirmation();
        Guid GetTicketNewStatusAfterConfirmation();
        Guid GetIdOfTicketStatusForConfirmOrder();
        Guid GetIdOfStatusForAddingToCart();
    }
}
