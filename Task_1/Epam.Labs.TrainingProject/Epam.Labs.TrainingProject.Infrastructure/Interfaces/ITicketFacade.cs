﻿using Epam.Labs.TrainingProject.AppCore.Entities;
using System;
using System.Collections.Generic;

namespace Epam.Labs.TrainingProject.Infrastructure.Interfaces
{
    public interface ITicketFacade
    {
        /// <summary>
        /// Get 4 selling tickets in status "important".
        /// </summary>
        /// <returns></returns>
        IEnumerable<Ticket> GetAllNotSoldImportantTickets();

        /// <summary>
        /// Get all selling tickets in all statuses except "important".
        /// </summary>
        /// <returns></returns>
        IEnumerable<Ticket> GetAllNotSoldNotImportantTickets();

        /// <summary>
        /// Get tickets of selected event from all existing tickets.
        /// </summary>
        /// <param name="id"></param>
        /// <param name="tickets"></param>
        /// <returns></returns>
        IEnumerable<Ticket> GetAllTicketsOfEventById(Guid? id, IEnumerable<Ticket> tickets);

        /// <summary>
        /// Get ticket by id.
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        Ticket GetTicketById(Guid id);

        /// <summary>
        /// Get amount of rows with tickets.
        /// </summary>
        /// <param name="ticketsAmount"></param>
        /// <returns></returns>
        int GetAmountOfRowsWithTickets(int ticketsAmount);

        /// <summary>
        /// Get tickets of selected seller and of specific status.
        /// </summary>
        /// <param name="sellerId"></param>
        /// <param name="status"></param>
        /// <returns></returns>
        IEnumerable<Ticket> GetTicketsBySellerAndStatus(Guid sellerId, string status);

        Ticket CreateTicket(Guid eventId, decimal price, Guid sellerId);
    }
}