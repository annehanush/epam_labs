﻿using Epam.Labs.TrainingProject.Infrastructure.Interfaces;
using Microsoft.AspNetCore.Mvc;

namespace Epam.Labs.TrainingProject.Web.Components
{
    public class CategoriesList : ViewComponent
    {
        ICategoryFacade _categoryFacade;

        public CategoriesList(ICategoryFacade categoryFacade)
        {
            _categoryFacade = categoryFacade;
        }

        public IViewComponentResult Invoke(string param)
        {
            var categories = _categoryFacade.GetAllCategoriesWhereEventsExist();

            if (param == "footerLinks")
            {
                return View("FooterLinks", categories);
            }
            else
            {
                return View(categories);
            }
        }
    }
}
