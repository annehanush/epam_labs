﻿using Epam.Labs.TrainingProject.Infrastructure.Interfaces;
using Microsoft.AspNetCore.Mvc;

namespace Epam.Labs.TrainingProject.Web.Components
{
    public class CategoriesOptionsList : ViewComponent
    {
        ICategoryFacade _categoryFacade;

        public CategoriesOptionsList(ICategoryFacade categoryFacade)
        {
            _categoryFacade = categoryFacade;
        }

        public IViewComponentResult Invoke(string param)
        {
            var categories = _categoryFacade.GetAllCategories();

            return View(categories);
        }
    }
}
