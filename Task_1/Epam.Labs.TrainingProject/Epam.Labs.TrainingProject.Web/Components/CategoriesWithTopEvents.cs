﻿using Epam.Labs.TrainingProject.Infrastructure.Interfaces;
using Microsoft.AspNetCore.Mvc;

namespace Epam.Labs.TrainingProject.Web.Components
{
    public class CategoriesWithTopEvents : ViewComponent
    {
        ICategoryFacade _categoryFacade;

        public CategoriesWithTopEvents(ICategoryFacade categoryFacade)
        {
            _categoryFacade = categoryFacade;
        }

        public IViewComponentResult Invoke()
        {
            var categoriesWithEvents = _categoryFacade.GetCategoriesWithTopEvents();
            return View(categoriesWithEvents);
        }
    }
}
