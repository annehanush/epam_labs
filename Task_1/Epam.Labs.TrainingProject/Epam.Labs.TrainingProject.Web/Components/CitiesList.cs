﻿using Epam.Labs.TrainingProject.Infrastructure.Interfaces;
using Microsoft.AspNetCore.Mvc;

namespace Epam.Labs.TrainingProject.Web.Components
{
    public class CitiesList : ViewComponent
    {
        IPlaceFacade _placeFacade;

        public CitiesList(IPlaceFacade placeFacade)
        {
            _placeFacade = placeFacade;
        }

        public IViewComponentResult Invoke(string param)
        {
            var cities = _placeFacade.GetAllCities();

            return View(cities);
        }
    }
}
