﻿using Epam.Labs.TrainingProject.Infrastructure.Interfaces;
using Microsoft.AspNetCore.Mvc;
using System.Linq;

namespace Epam.Labs.TrainingProject.Web.Components
{
    public class EventsList : ViewComponent
    {
        IEventFacade _eventFacade;

        public EventsList(IEventFacade eventFacade)
        {
            _eventFacade = eventFacade;
        }

        public IViewComponentResult Invoke(string param)
        {
            var events = _eventFacade.GetAllEvents().OrderBy(e => e.Venue.City.Name);

            return View(events);
        }
    }
}
