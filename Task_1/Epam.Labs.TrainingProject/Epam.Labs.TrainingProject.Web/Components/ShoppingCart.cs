﻿using Epam.Labs.TrainingProject.Infrastructure.Interfaces;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.ViewComponents;
using Microsoft.AspNetCore.Html;

namespace Epam.Labs.TrainingProject.Web.Components
{
    public class ShoppingCart : ViewComponent
    {
        IAccountFacade _accountFacade;

        public ShoppingCart(IAccountFacade accountFacade)
        {
            _accountFacade = accountFacade;
        }

        public IViewComponentResult Invoke()
        {
            var itemsAmount = _accountFacade.GetUserCartItemsAmount(User.Identity.Name);
            return View(itemsAmount);
        }
    }
}
