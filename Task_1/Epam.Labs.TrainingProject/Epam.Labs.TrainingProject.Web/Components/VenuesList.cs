﻿using Epam.Labs.TrainingProject.Infrastructure.Interfaces;
using Microsoft.AspNetCore.Mvc;

namespace Epam.Labs.TrainingProject.Web.Components
{
    public class VenuesList : ViewComponent
    {
        IPlaceFacade _placeFacade;

        public VenuesList(IPlaceFacade placeFacade)
        {
            _placeFacade = placeFacade;
        }

        public IViewComponentResult Invoke(string param)
        {
            var venues = _placeFacade.GetAllVenues();

            return View(venues);
        }
    }
}
