﻿using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Epam.Labs.TrainingProject.Web.Models.AccountViewModels;
using Epam.Labs.TrainingProject.AppCore.Entities;
using System.Security.Claims;
using Epam.Labs.TrainingProject.Infrastructure.Interfaces;

namespace Epam.Labs.TrainingProject.Web.Controllers
{
    public class AccountController : Controller
    {
        IAccountFacade _accountFacade;
        ICultureFacade _cultureFacade;
        IDbInsertFacade _dbInsertFacade;

        public AccountController(IAccountFacade accountFacade, ICultureFacade cultureFacade, IDbInsertFacade dbInsertFacade)
        {
            _accountFacade = accountFacade;
            _cultureFacade = cultureFacade;
            _dbInsertFacade = dbInsertFacade;
        }

        /// <summary>
        /// Log in page.
        /// </summary>
        /// <returns></returns>
        [HttpGet]
        public IActionResult Login()
        {
            // if user is authenticated he will be redirected to index page (kind of protection)
            if (User.Identity.IsAuthenticated)
            {
                return RedirectToAction("Index", "Home");
            }
            else
            {
                return View();
            }
        }

        /// <summary>
        /// Log in.
        /// </summary>
        /// <param name="model"></param>
        /// <returns></returns>
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> Login(LoginModel model)
        {
            if (ModelState.IsValid)
            {
                AuthenticationData auth = _accountFacade.GetAuthenticationByUsernameAmdPassword(model.Username, model.Password);

                if (auth != null)
                {
                    await Authenticate(auth);

                    return RedirectToAction("SetLanguage", "Home", new { cultureName = auth.User.Culture.Name });
                }
                ModelState.AddModelError("", "Incorrect login or(and) password");
            }
            
            return View(model);
        }

        /// <summary>
        /// Sign up page.
        /// </summary>
        /// <returns></returns>
        [HttpGet]
        public IActionResult Register()
        {
            // if user is authenticated he will be redirected to index page (kind of protection)
            if (User.Identity.IsAuthenticated)
            {
                return RedirectToAction("Index", "Home");
            }
            else
            {
                return View();
            }
        }

        /// <summary>
        /// Registration.
        /// </summary>
        /// <param name="model"></param>
        /// <returns></returns>
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> Register(RegisterModel model)
        {
            if (ModelState.IsValid)
            {
                AuthenticationData authUser = _accountFacade.GetAuthenticationByUsername(model.Username);
                if (authUser == null)
                {
                    Role userRole = _accountFacade.GetRoleByName("user");

                    var newAppUser = new User
                    {
                        Id = Guid.NewGuid(),
                        FirstName = model.FirstName,
                        LastName = model.LastName,
                        Address = "",
                        Phone = "",
                        CultureId = _cultureFacade.GetCultureByName(model.Localization).Id
                    };
                    AuthenticationData auth = new AuthenticationData
                    {
                        Username = model.Username,
                        Email = model.Email,
                        Password = model.Password,
                        RoleId = userRole.Id,
                        UserId = newAppUser.Id
                    };

                    _dbInsertFacade.AddNewUser(newAppUser, auth);

                    // authentication
                    await Authenticate(auth);

                    return RedirectToAction("SetLanguage", "Home", new { cultureName = model.Localization });
                }
                else
                    ModelState.AddModelError("", "User with such username already exists.");
            }
            
            return View(model);
        }

        private async Task Authenticate(AuthenticationData authUser)
        {
            var roleName = _accountFacade.GetRoleById(authUser.RoleId).Name;

            // claim creation
            var claims = new List<Claim>
            {
                new Claim(ClaimsIdentity.DefaultNameClaimType, authUser.Username),
                new Claim(ClaimsIdentity.DefaultRoleClaimType, roleName)
            };

            // ClaimsIdentity object creation
            ClaimsIdentity id = new ClaimsIdentity(claims, "ApplicationCookie", ClaimsIdentity.DefaultNameClaimType, ClaimsIdentity.DefaultRoleClaimType);
            
            // set authentication cookies
            await HttpContext.Authentication.SignInAsync("Cookies", new ClaimsPrincipal(id));
        }

        /// <summary>
        /// Log out.
        /// </summary>
        /// <returns></returns>
        public async Task<IActionResult> Logout()
        {
            await HttpContext.Authentication.SignOutAsync("Cookies");
            return RedirectToAction("Login", "Account");
        }
    }
}