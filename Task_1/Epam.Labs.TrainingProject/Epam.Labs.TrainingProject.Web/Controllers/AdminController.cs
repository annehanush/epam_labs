﻿using Microsoft.AspNetCore.Mvc;
using Epam.Labs.TrainingProject.Infrastructure.Interfaces;
using Microsoft.AspNetCore.Authorization;
using System;
using Epam.Labs.TrainingProject.Web.Models.DbModels;
using Epam.Labs.TrainingProject.AppCore.Entities;
using Microsoft.AspNetCore.Hosting;
using System.IO;

namespace Epam.Labs.TrainingProject.Web.Controllers
{
    [Authorize(Roles = "admin")]
    public class AdminController : Controller
    {
        IAccountFacade _accounFacade;
        IPlaceFacade _placeFacade;
        IEventFacade _eventFacade;
        IHostingEnvironment _appEnvironment;

        IDbInsertFacade _dbInserFacade;
        IDbUpdateFacade _dbUpdateFacade;
        IDbDeleteFacade _dbDeleteFacade;

        public AdminController(IAccountFacade accountFacade, IPlaceFacade placeFacade, IEventFacade eventFacade, 
            IHostingEnvironment appEnvironment, IDbInsertFacade dbInserFacade, IDbUpdateFacade dbUpdateFacade,
            IDbDeleteFacade dbDeleteFacade)
        {
            _accounFacade = accountFacade;
            _placeFacade = placeFacade;
            _eventFacade = eventFacade;
            _appEnvironment = appEnvironment;
            _dbInserFacade = dbInserFacade;
            _dbUpdateFacade = dbUpdateFacade;
            _dbDeleteFacade = dbDeleteFacade;
        }
        
        public IActionResult AdminArea()
        {
            var appUsers = _accounFacade.GetAllAppUsers(User.Identity.Name);

            return View(appUsers);
        }

        public RedirectToActionResult GrantAdminRights(string username)
        {
            var authUser = _accounFacade.GetAuthenticationByUsername(username);

            _dbUpdateFacade.GrantRevokeAdminRights(authUser, Guid.Parse("535c52ba-a706-4d8e-91c1-17b748ebdda7"));

            return RedirectToAction("AdminArea");
        }

        public RedirectToActionResult RevokeAdminRights(string username)
        {
            var authUser = _accounFacade.GetAuthenticationByUsername(username);

            _dbUpdateFacade.GrantRevokeAdminRights(authUser, Guid.Parse("157964cf-6b71-4f72-a7c3-b243158fec68"));

            return RedirectToAction("AdminArea");
        }

        // CITIES OPTIONS

        public IActionResult Cities()
        {
            var cities = _placeFacade.GetAllCities();

            return View(cities);
        }

        [HttpGet]
        public IActionResult AddCity()
        {
            return View();
        }

        [HttpPost]
        public IActionResult AddCity(AddEditCityModel model)
        {
            if (ModelState.IsValid)
            {
                var city = _placeFacade.GetCityByName(model.Name);

                if (city == null)
                {
                    var newCity = new City()
                    {
                        Id = Guid.NewGuid(),
                        Name = model.Name
                    };

                    _dbInserFacade.AddCity(newCity);

                    return RedirectToAction("Cities");
                }

                ModelState.AddModelError("", "City with the same name already exists.");
            }

            return View(model);
        }

        [HttpGet]
        public IActionResult EditCity(Guid cityId)
        {
            ViewBag.City = _placeFacade.GetCityById(cityId);
            return View();
        }

        [HttpPost]
        public IActionResult EditCity(AddEditCityModel model)
        {
            var city = _placeFacade.GetCityById(model.IdOfCurrentCity);

            if (ModelState.IsValid)
            {
                if (city.Name != model.Name)
                {
                    _dbUpdateFacade.EditCity(city, model.Name);

                    return RedirectToAction("Cities");
                }

                ModelState.AddModelError("", "To change city name you should enter new name.");
            }

            ViewBag.City = city;
            return View(model);
        }

        public RedirectToActionResult RemoveCity(Guid id)
        {
            var city = _placeFacade.GetCityById(id);

            _dbDeleteFacade.RemoveCity(city);

            return RedirectToAction("Cities");
        }

        // VENUES OPTIONS

        public IActionResult Venues()
        {
            var venues = _placeFacade.GetAllVenues();

            return View(venues);
        }

        [HttpGet]
        public IActionResult AddVenue()
        {
            return View();
        }

        [HttpPost]
        public IActionResult AddVenue(AddEditVenueModel model)
        {
            if (model.CityId != null)
            {
                if (ModelState.IsValid)
                {
                    var venue = _placeFacade.GetVenueByName(model.Name);

                    if (venue == null)
                    {
                        var newVenue = new Venue()
                        {
                            Id = Guid.NewGuid(),
                            Name = model.Name,
                            Address = model.Address,
                            CityId = model.CityId
                        };

                        _dbInserFacade.AddVenue(newVenue);

                        return RedirectToAction("Venues");
                    }

                    ModelState.AddModelError("", "Venue with the same name already exists.");
                }
            }
            ModelState.AddModelError("", "City field is required.");

            return View(model);
        }

        [HttpGet]
        public IActionResult EditVenue(Guid venueId)
        {
            ViewBag.Venue = _placeFacade.GetVenueById(venueId);
            return View();
        }

        [HttpPost]
        public IActionResult EditVenue(AddEditVenueModel model)
        {
            var venue = _placeFacade.GetVenueById(model.IdOfCurrentVenue);

            if (ModelState.IsValid)
            {
                _dbUpdateFacade.EditVenue(venue, model.Name, model.CityId);

                return RedirectToAction("Venues");
            }

            ViewBag.Venue = venue;
            return View(model);
        }

        public RedirectToActionResult RemoveVenue(Guid id)
        {
            var venue = _placeFacade.GetVenueById(id);

            _dbDeleteFacade.RemoveVenue(venue);

            return RedirectToAction("Venues");
        }

        // EVENTS OPTIONS

        public IActionResult Events()
        {
            var events = _eventFacade.GetAllEvents();

            return View(events);
        }

        [HttpGet]
        public IActionResult AddEvent()
        {
            return View();
        }

        [HttpPost]
        public IActionResult AddEvent(AddEditEventModel model)
        {
            if (model.VenueId != null)
            {
                if (model.CategoryId != null)
                {
                    if (model.Image != null)
                    {
                        if (ModelState.IsValid)
                        {
                            var _event = _eventFacade.GetEventByName(model.Name);

                            if (_event == null)
                            {
                                string path = "/images/" + model.Image.FileName;

                                using (var fileStream = new FileStream(_appEnvironment.WebRootPath + path, FileMode.Create))
                                {
                                    model.Image.CopyTo(fileStream);
                                }

                                var newEvent = new Event()
                                {
                                    Id = Guid.NewGuid(),
                                    Name = model.Name,
                                    Date = model.Date,
                                    Banner = path,
                                    Description = model.Description,
                                    VenueId = model.VenueId,
                                    CategoryId = model.CategoryId
                                };

                                _dbInserFacade.AddEvent(newEvent);

                                return RedirectToAction("Events");
                            }
                            
                            ModelState.AddModelError("", "Event with the same name already exists.");
                        }
                    }
                    else
                    {
                        ModelState.AddModelError("", "Image is required.");
                    }
                }
                else
                {
                    ModelState.AddModelError("", "Category field is required.");
                }
            }
            else
            {
                ModelState.AddModelError("", "Venue field is required.");
            }

            return View(model);
        }

        [HttpGet]
        public IActionResult EditEvent(Guid eventId)
        {
            ViewBag.Event = _eventFacade.GetEventById(eventId);
            return View();
        }

        [HttpPost]
        public IActionResult EditEvent(AddEditEventModel model)
        {
            var newEvent = _eventFacade.GetEventById(model.IdOfCurrentEvent);

            if (model.VenueId != null)
            {
                if (model.CategoryId != null)
                {
                    if (ModelState.IsValid)
                    {
                        _dbUpdateFacade.EditEvent(newEvent, model.Name, model.Date, model.VenueId, model.CategoryId, model.Description);

                        return RedirectToAction("Events");
                    }
                }
                else
                {
                    ModelState.AddModelError("", "Category field is required.");
                }
            }
            else
            {
                ModelState.AddModelError("", "Venue field is required.");
            }

            ViewBag.Event = newEvent;
            return View(model);
        }

        public RedirectToActionResult RemoveEvent(Guid id)
        {
            var _event = _eventFacade.GetEventById(id);

            _dbDeleteFacade.RemoveEvent(_event);

            return RedirectToAction("Events");
        }

    }
}
