﻿using System;
using System.Collections.Generic;
using System.Linq;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Authorization;
using Epam.Labs.TrainingProject.Infrastructure.Interfaces;
using Epam.Labs.TrainingProject.Web.Models.ViewModels;
using Epam.Labs.TrainingProject.Web.Models.AccountViewModels;
using Epam.Labs.TrainingProject.Infrastructure.DTO;
using Epam.Labs.TrainingProject.Web.Models.DbModels;

namespace Epam.Labs.TrainingProject.Web.Controllers
{
    public class AppUsersController : Controller
    {
        IOrderFacade _orderFacade;
        IAccountFacade _accountFacade;
        ICultureFacade _cultureFacade;
        ITicketFacade _ticketFacade;
        IStatusFacade _statusFacade;

        IDbInsertFacade _dbInsertFacade;
        IDbUpdateFacade _dbUpdateFacade;
        IDbDeleteFacade _dbDeleteFacade;

        public AppUsersController(IOrderFacade orderFacade, IAccountFacade accountFacade, 
            ICultureFacade cultureFacade, ITicketFacade ticketFacade, IStatusFacade statusFacade,
            IDbInsertFacade dbInsertFacade, IDbUpdateFacade dbUpdateFacade, IDbDeleteFacade dbDeleteFacade)
        {
            _orderFacade = orderFacade;
            _accountFacade = accountFacade;
            _cultureFacade = cultureFacade;
            _ticketFacade = ticketFacade;
            _statusFacade = statusFacade;
            _dbInsertFacade = dbInsertFacade;
            _dbUpdateFacade = dbUpdateFacade;
            _dbDeleteFacade = dbDeleteFacade;
        }
        
        [Authorize]
        public IActionResult Cart()
        {
            var buyerId = _accountFacade.GetAuthenticationByUsername(User.Identity.Name).UserId;
            var items = _orderFacade.GetOrdersByBuyerAndStatus(buyerId, "Waiting for buyer Confirmation");

            var viewModel = new ViewModelCart()
            {
                CartItems = items,
                CartItemsAmount = items.Count(),
                TotalCost = items.Sum(i => i.Ticket.Price)
            };

            return View(viewModel);
        }
        
        public RedirectToActionResult RemoveFromCart(Guid Id)
        {
            _dbDeleteFacade.RemoveOrderFromCart(Id);

            return RedirectToAction("Cart");
        }
        
        public IActionResult ConfirmOrder(List<Guid> ordersId)
        {
            var statusId = _statusFacade.GetIdOfStatusForOrderConfirmation();
            var ticketStatusId = _statusFacade.GetIdOfTicketStatusForConfirmOrder();

            foreach (var orderId in ordersId)
            {
                var order = _orderFacade.GetOrderById(orderId);
                var ticketId = order.TicketId.Value;

                _dbUpdateFacade.ConfirmOrder(order, statusId);
                _dbUpdateFacade.ConfirmOrderTicketChanges(ticketId, ticketStatusId);
            }
            
            return RedirectToAction("UserOrders");
        }

        /// <summary>
        /// User info page.
        /// </summary>
        /// <returns></returns>
        [Authorize]
        public IActionResult UserInfo()
        {
            var currentAuthUser = _accountFacade.GetAuthenticationByUsername(User.Identity.Name);

            return View(currentAuthUser);
        }

        [HttpGet]
        public IActionResult EditUserInfo()
        {
            ViewBag.UserToChange = _accountFacade.GetAuthenticationByUsername(User.Identity.Name);
            return View();
        }

        [HttpPost]
        public IActionResult EditUserInfo(EditUserInfoModel model)
        {
            var auth = _accountFacade.GetAuthenticationByUsername(User.Identity.Name);

            if (ModelState.IsValid)
            {
                var user = auth.User;

                var newModel = new EditUserInfoDTO()
                {
                    FirstName = model.FirstName,
                    LastName = model.LastName,
                    Email = model.Email,
                    Address = model.Address,
                    Phone = model.Phone,
                    CultureId = _cultureFacade.GetCultureByName(model.Culture).Id
                };

                _dbUpdateFacade.EditUserInfo(newModel, auth, user);

                return RedirectToAction("UserInfo", "AppUsers");
            }

            ViewBag.UserToChange = auth;
            return View(model);
        }

        /// <summary>
        /// User tickets page (list of tickets that user is selling).
        /// </summary>
        /// <returns></returns>
        [Authorize]
        public IActionResult UserTickets()
        {
            Guid currentUserId = _accountFacade.GetAuthenticationByUsername(User.Identity.Name).User.Id;

            UserTicketsViewModel viewModel = new UserTicketsViewModel
            {
                WaitingConfirmationTickets = _ticketFacade.GetTicketsBySellerAndStatus(currentUserId, "Waiting for seller Confirmation"),
                SellingTickets = _ticketFacade.GetTicketsBySellerAndStatus(currentUserId, "Selling"),
                SoldTickets = _ticketFacade.GetTicketsBySellerAndStatus(currentUserId, "Sold")
            };

            ViewBag.RowsAmountWFCTickets = _ticketFacade.GetAmountOfRowsWithTickets(viewModel.WaitingConfirmationTickets.Count());
            ViewBag.RowsAmountSellingTickets = _ticketFacade.GetAmountOfRowsWithTickets(viewModel.SellingTickets.Count());
            ViewBag.RowsAmountSoldTickets = _ticketFacade.GetAmountOfRowsWithTickets(viewModel.SoldTickets.Count());

            return View(viewModel);
        }

        /// <summary>
        /// User orders page (tickets that user is going to buy).
        /// </summary>
        /// <returns></returns>
        [Authorize]
        public IActionResult UserOrders()
        {
            Guid currentUserId = _accountFacade.GetAuthenticationByUsername(User.Identity.Name).User.Id;

            UserOrdersViewModel viewModel = new UserOrdersViewModel
            {
                WaitingForConfirmationOrders = _orderFacade.GetOrdersByBuyerAndStatus(currentUserId, "Waiting for seller Confirmation"),
                ConfirmedOrders = _orderFacade.GetOrdersByBuyerAndStatus(currentUserId, "Confirmed"),
                RejectedOrders = _orderFacade.GetOrdersByBuyerAndStatus(currentUserId, "Rejected")
            };

            ViewBag.RowsAmountWFCOrders = _ticketFacade.GetAmountOfRowsWithTickets(viewModel.WaitingForConfirmationOrders.Count());
            ViewBag.RowsAmountConfirmedOrders = _ticketFacade.GetAmountOfRowsWithTickets(viewModel.ConfirmedOrders.Count());
            ViewBag.RowsAmountRejectedOrders = _ticketFacade.GetAmountOfRowsWithTickets(viewModel.RejectedOrders.Count());

            return View(viewModel);
        }

        public IActionResult WfCTicketOrders(Guid ticketId)
        {
            var statusId = _statusFacade.GetStatusByItsName("Waiting for seller Confirmation").Id;
            var orders = _orderFacade.GetAllOrdersOfTicketById(ticketId, statusId);

            return View(orders);
        }

        /// <summary>
        /// Confirm ticket ordering.
        /// </summary>
        /// <param name="ticketId"></param>
        /// <returns></returns>
        public RedirectToActionResult ConfirmTicketOrdering(Guid id)
        {
            var order = _orderFacade.GetOrderById(id);

            var ticketNewStatusId = _statusFacade.GetTicketNewStatusAfterConfirmation();
            var orderNewStatusId = _statusFacade.GetOrderNewStatusAfterConfirmation();
            var otherOrdersStatusId = _statusFacade.GetStatusByItsName("Rejected").Id;

            _dbUpdateFacade.ConfirmTicketOrdering(order.TicketId.Value, id, ticketNewStatusId, orderNewStatusId, otherOrdersStatusId);

            return RedirectToAction("UserTickets");
        }

        [HttpGet]
        public IActionResult RejectTicketOrdering(Guid id)
        {
            ViewBag.OrderId = id;
            return View();
        }

        [HttpPost]
        public IActionResult RejectTicketOrdering(RejectionModel model)
        {
            if (ModelState.IsValid)
            {
                if (model.Comment == null)
                {
                    model.Comment = "Ticket has already been sold.";
                }

                var order = _orderFacade.GetOrderById(model.Id);

                var ticketNewStatusId = _statusFacade.GetStatusByItsName("Selling").Id;
                var orderNewStatusId = _statusFacade.GetStatusByItsName("Rejected").Id;

                _dbUpdateFacade.RejectTicketOrdering(order.TicketId.Value, model.Id, ticketNewStatusId, orderNewStatusId, model.Comment);

                return RedirectToAction("UserTickets");
            }

            return View(model);
        }

        [HttpGet]
        [Authorize]
        public IActionResult AddNewTicket()
        {
            return View();
        }

        [HttpPost]
        public IActionResult AddNewTicket(AddTicketModel model)
        {
            if (ModelState.IsValid)
            {
                Guid currentUserId = _accountFacade.GetAuthenticationByUsername(User.Identity.Name).User.Id;

                var ticket = _ticketFacade.CreateTicket(model.EventId, model.Price, currentUserId);
                _dbInsertFacade.AddNewTicket(ticket);

                return RedirectToAction("UserTickets");
            }

            return View(model);
        }
    }
}