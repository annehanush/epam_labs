﻿using Microsoft.AspNetCore.Mvc;
using System.Collections.Generic;
using Epam.Labs.TrainingProject.AppCore.Entities;
using System.Linq;
using Epam.Labs.TrainingProject.Web.Models.PaginationViewModels;
using System;
using System.Globalization;
using Microsoft.AspNetCore.Localization;
using Microsoft.AspNetCore.Http;
using Epam.Labs.TrainingProject.Infrastructure.Interfaces;
using Epam.Labs.TrainingProject.Web.Models.ViewModels;

namespace Epam.Labs.TrainingProject.Web.Controllers
{
    public class HomeController : Controller
    {
        IEventFacade _eventFacade;
        ICultureFacade _cultureFacade;
        ICategoryFacade _categoryFacade;

        public HomeController(IEventFacade eventFacade, ICultureFacade cultureFacade, ICategoryFacade categoryFacade)
        {
            _eventFacade = eventFacade;
            _cultureFacade = cultureFacade;
            _categoryFacade = categoryFacade;
        }

        /// <summary>
        /// Index (main) page.
        /// </summary>
        /// <returns></returns>
        public IActionResult Index()
        {
            ViewModelIndex viewModel = new ViewModelIndex
            {
                UpcomingEvents = _eventFacade.GetUpcomingEvents()
            };

            // cultures
            ViewBag.CurrentCulture = CultureInfo.CurrentCulture.TwoLetterISOLanguageName;
            ViewBag.Cultures = _cultureFacade.GetAllCultures();

            return View(viewModel);
        }


        /// <summary>
        /// All existing events or all the events of selected category.
        /// </summary>
        /// <param name="page">Page number.</param>
        /// <param name="categoryId">Id of selected category.</param>
        /// <returns></returns>
        public IActionResult Events(int page = 1, Guid? categoryId = null)
        {
            // amount of elements(events) on page
            var pageSize = 9;
            var categoryOrNot = "all";

            IEnumerable<Event> events = _eventFacade.GetAllEvents();

            if (categoryId != null)
            {
                events = _eventFacade.GetAllEventsOfCategory(categoryId, events);
                categoryOrNot = _categoryFacade.GetCategory(categoryId.Value).Name;
            }

            var count = events.Count();
            var items = events.Skip((page - 1) * pageSize).Take(pageSize).ToList();

            PageViewModel pageViewModel = new PageViewModel(count, page, pageSize);
            ViewModelEvents viewModel = new ViewModelEvents
            {
                PageViewModel = pageViewModel,
                CategoryName = categoryOrNot,
                Events = items,
                RowsAmount = _eventFacade.GetAmountOfRowsWithEvents(items.Count)
            };

            return View(viewModel);
        }

        /// <summary>
        /// Selected event details.
        /// </summary>
        /// <param name="Id">Id of selected event.</param>
        /// <returns></returns>
        public IActionResult Event(Guid Id)
        {
            var _event = _eventFacade.GetFormattedEvent(Id);

            ViewModelEvent viewModel = new ViewModelEvent
            {
                CurrentEvent = _event,
                TopEventsOfCategory = _eventFacade.GetTopEventsOfCategory(_event.Category.Id)
            };

            return View(viewModel);
        }

        /// <summary>
        /// Setting of selected language.
        /// </summary>
        /// <param name="cultureName">Selected language name (culture).</param>
        /// <param name="returnUrl"></param>
        /// <returns></returns>
        public IActionResult SetLanguage(string cultureName, string returnUrl)
        {
            Response.Cookies.Append(
                CookieRequestCultureProvider.DefaultCookieName,
                CookieRequestCultureProvider.MakeCookieValue(new RequestCulture(cultureName)),
                new CookieOptions { Expires = DateTimeOffset.UtcNow.AddYears(1) }
            );
            
            if (returnUrl != null)
            {
                return LocalRedirect(returnUrl);
            }
            else
            {
                return RedirectToAction("Index", "Home");
            }
        }
    }
}