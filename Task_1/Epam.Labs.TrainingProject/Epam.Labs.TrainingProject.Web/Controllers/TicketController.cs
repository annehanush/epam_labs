﻿using System;
using System.Linq;
using Microsoft.AspNetCore.Mvc;
using Epam.Labs.TrainingProject.Web.Models.PaginationViewModels;
using Epam.Labs.TrainingProject.Infrastructure.Interfaces;

namespace Epam.Labs.TrainingProject.Web.Controllers
{
    // shopping cart options
    public enum CartOptions
    {
        Error,
        Success
    }

    public class TicketController : Controller
    {
        IEventFacade _eventFacade;
        ITicketFacade _ticketFacade;
        IAccountFacade _accountFacade;
        IStatusFacade _statusFacade;
        IDbInsertFacade _dbInsertFacade;

        public TicketController(IEventFacade eventFacade, ITicketFacade ticketFacade, IAccountFacade accountFacade, 
            IStatusFacade statusFacade, IDbInsertFacade dbInsertFacade)
        {
            _eventFacade = eventFacade;
            _ticketFacade = ticketFacade;
            _accountFacade = accountFacade;
            _statusFacade = statusFacade;
            _dbInsertFacade = dbInsertFacade;
        }

        /// <summary>
        /// Tickets page.
        /// </summary>
        /// <param name="eventId">Id of event, for what to show list of tickets.</param>
        /// <param name="page">Page number.</param>
        /// <param name="sortBy">Type of sort.</param>
        /// <returns></returns>
        public IActionResult Tickets(Guid? eventId = null, int page = 1, Sorting sortBy = Sorting.Popularity, CartOptions? cartOption = null)
        {
            // amount of elements (tickets) on page
            var pageSize = 12;
            var currentTitle = "";
            var otherSorting = Sorting.Popularity;

            // all tickets (but not important)
            var tickets = _ticketFacade.GetAllNotSoldNotImportantTickets();
            // important tickets
            var importantTickets = _ticketFacade.GetAllNotSoldImportantTickets();

            if (eventId != null)
            {
                tickets = _ticketFacade.GetAllTicketsOfEventById(eventId, tickets);
                currentTitle = _eventFacade.GetEventNameById(eventId).ToUpper();
            }
            else
            {
                // Sorting
                if (sortBy == Sorting.Popularity)
                {
                    tickets = tickets.OrderBy(t => t.TicketType);
                    otherSorting = Sorting.Date;
                }
                else
                {
                    tickets = tickets.OrderBy(t => t.Event.Date);
                    otherSorting = Sorting.Popularity;
                }
            }

            var count = tickets.Count();
            var items = tickets.Skip((page - 1) * pageSize).Take(pageSize).ToList();

            var pageViewModel = new PageViewModel(count, page, pageSize);
            var viewModel = new TicketViewModel
            {
                PageViewModel = pageViewModel,
                Tickets = items,
                ImportantTickets = importantTickets,
                CurrentTitle = currentTitle,
                OtherSorting = otherSorting,
                RowsAmount = _ticketFacade.GetAmountOfRowsWithTickets(items.Count)
            };
            
            ViewBag.CartOption = cartOption;

            return View(viewModel);
        }

        /// <summary>
        /// Add ticket to cart.
        /// </summary>
        /// <param name="ticketId">Id of selected ticket.</param>
        /// <returns></returns>
        public RedirectToActionResult AddTicketToCart(Guid ticketId)
        {
            if (User.Identity.IsAuthenticated)
            {
                var buyerId = _accountFacade.GetAuthenticationByUsername(User.Identity.Name).UserId;
                var sellerId = _ticketFacade.GetTicketById(ticketId).Seller.Id;

                if (buyerId == sellerId)
                {
                    return RedirectToAction("Tickets", "Ticket", new { cartOption = 0 });
                }

                var statusId = _statusFacade.GetIdOfStatusForAddingToCart();

                _dbInsertFacade.AddNewOrderToCart(ticketId, buyerId, statusId);
                return RedirectToAction("Tickets", "Ticket", new { cartOption = 1 });
            }
            else
            {
                return RedirectToAction("Login", "Account");
            }
        }
    }
}