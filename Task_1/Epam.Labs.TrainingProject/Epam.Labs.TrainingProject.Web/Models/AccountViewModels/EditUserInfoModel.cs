﻿using System.ComponentModel.DataAnnotations;

namespace Epam.Labs.TrainingProject.Web.Models.AccountViewModels
{
    public class EditUserInfoModel
    {
        [Required(ErrorMessage = "First name field is required")]
        public string FirstName { get; set; }
        [Required(ErrorMessage = "Last name field is required")]
        public string LastName { get; set; }

        [Required(ErrorMessage = "Email field is required")]
        [EmailAddress(ErrorMessage = "Invalid Email Address")]
        public string Email { get; set; }
        
        public string Address { get; set; }
        public string Phone { get; set; }

        [Required(ErrorMessage = "Culture field is required")]
        public string Culture { get; set; }
    }
}
