﻿using System.ComponentModel.DataAnnotations;

namespace Epam.Labs.TrainingProject.Web.Models.AccountViewModels
{
    public class LoginModel
    {
        [Required(ErrorMessage = "Username field is required")]
        public string Username { get; set; }

        [Required(ErrorMessage = "Password field is required")]
        [DataType(DataType.Password)]
        public string Password { get; set; }
    }
}
