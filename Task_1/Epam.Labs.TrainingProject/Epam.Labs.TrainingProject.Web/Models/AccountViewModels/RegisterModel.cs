﻿using System.ComponentModel.DataAnnotations;

namespace Epam.Labs.TrainingProject.Web.Models.AccountViewModels
{
    public class RegisterModel
    {

        [Required(ErrorMessage = "First name field is required")]
        public string FirstName { get; set; }

        [Required(ErrorMessage = "Last name field is required")]
        public string LastName { get; set; }

        [Required(ErrorMessage = "Username field is required")]
        // [UsernameExistence(ErrorMessage = "User with entered username already exists")]
        public string Username { get; set; }

        [Required(ErrorMessage = "Email field is required")]
        [EmailAddress(ErrorMessage = "Invalid Email Address")]
        // [EmailExistence(ErrorMessage = "User with entered email already exists")]
        public string Email { get; set; }

        [Required(ErrorMessage = "Culture field is required")]
        // [CorrectLocalization(ErrorMessage = "No such localization.")]
        public string Localization { get; set; }

        [Required(ErrorMessage = "Password field is required")]
        [StringLength(30, ErrorMessage = "Password must be at least 4 characters long.", MinimumLength = 4)]
        [DataType(DataType.Password)]
        public string Password { get; set; }

        [DataType(DataType.Password)]
        [Compare("Password", ErrorMessage = "Passwords do not match")]
        public string ConfirmPassword { get; set; }
    }
}
