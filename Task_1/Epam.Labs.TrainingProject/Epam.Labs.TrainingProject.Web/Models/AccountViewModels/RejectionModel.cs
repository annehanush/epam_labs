﻿using System;

namespace Epam.Labs.TrainingProject.Web.Models.AccountViewModels
{
    public class RejectionModel
    {
        public Guid Id { get; set; }
        public string Comment { get; set; }
    }
}
