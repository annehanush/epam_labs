﻿using Epam.Labs.TrainingProject.AppCore.Entities;
using System.Collections.Generic;

namespace Epam.Labs.TrainingProject.Web.Models.AccountViewModels
{
    public class UserOrdersViewModel
    {
        public IEnumerable<Order> WaitingForConfirmationOrders { get; set; }
        public IEnumerable<Order> ConfirmedOrders { get; set; }
        public IEnumerable<Order> RejectedOrders { get; set; }
    }
}
