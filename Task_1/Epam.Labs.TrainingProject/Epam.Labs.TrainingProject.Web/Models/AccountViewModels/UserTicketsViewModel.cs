﻿using Epam.Labs.TrainingProject.AppCore.Entities;
using System.Collections.Generic;

namespace Epam.Labs.TrainingProject.Web.Models.AccountViewModels
{
    public class UserTicketsViewModel
    {
        public IEnumerable<Ticket> WaitingConfirmationTickets { get; set; }
        public IEnumerable<Ticket> SellingTickets { get; set; }
        public IEnumerable<Ticket> SoldTickets { get; set; }
    }
}
