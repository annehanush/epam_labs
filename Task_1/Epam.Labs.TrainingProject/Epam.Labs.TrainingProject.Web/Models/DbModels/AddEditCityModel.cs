﻿using System;
using System.ComponentModel.DataAnnotations;

namespace Epam.Labs.TrainingProject.Web.Models.DbModels
{
    public class AddEditCityModel
    {
        public Guid IdOfCurrentCity { get; set; }

        [Required(ErrorMessage = "Name field is required")]
        public string Name { get; set; }
    }
}
