﻿using Microsoft.AspNetCore.Http;
using System;
using System.ComponentModel.DataAnnotations;

namespace Epam.Labs.TrainingProject.Web.Models.DbModels
{
    public class AddEditEventModel
    {
        public Guid IdOfCurrentEvent { get; set; }

        [Required(ErrorMessage = "Name field is required")]
        public string Name { get; set; }

        [Required(ErrorMessage = "Date field is required")]
        public DateTime Date { get; set; }
        
        public IFormFile Image { get; set; }
        
        public Guid VenueId { get; set; }
        
        public Guid CategoryId { get; set; }

        public string Description { get; set; }
    }
}
