﻿using System;
using System.ComponentModel.DataAnnotations;

namespace Epam.Labs.TrainingProject.Web.Models.DbModels
{
    public class AddEditVenueModel
    {
        public Guid IdOfCurrentVenue { get; set; }

        [Required(ErrorMessage = "Name field is required")]
        public string Name { get; set; }

        [Required(ErrorMessage = "Address field is required")]
        public string Address { get; set; }

        [Required(ErrorMessage = "City field is required")]
        public Guid CityId { get; set; }
    }
}
