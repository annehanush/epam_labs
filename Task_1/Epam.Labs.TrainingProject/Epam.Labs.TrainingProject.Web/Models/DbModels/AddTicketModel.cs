﻿using System;
using System.ComponentModel.DataAnnotations;

namespace Epam.Labs.TrainingProject.Web.Models.DbModels
{
    public class AddTicketModel
    {
        [Required(ErrorMessage = "Price field is required")]
        public decimal Price { get; set; }

        [Required(ErrorMessage = "Event field is required")]
        public Guid EventId { get; set; }
    }
}
