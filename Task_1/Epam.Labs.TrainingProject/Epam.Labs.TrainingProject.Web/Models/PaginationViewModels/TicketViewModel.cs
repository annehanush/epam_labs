﻿using Epam.Labs.TrainingProject.AppCore.Entities;
using System.Collections.Generic;

namespace Epam.Labs.TrainingProject.Web.Models.PaginationViewModels
{
    // types of sorting
    public enum Sorting
    {
        Popularity,
        Date
    }

    public class TicketViewModel
    {
        public IEnumerable<Ticket> Tickets { get; set; }
        public IEnumerable<Ticket> ImportantTickets { get; set; }
        public PageViewModel PageViewModel { get; set; }
        public string CurrentTitle { get; set; }
        public Sorting OtherSorting { get; set; }
        public int RowsAmount { get; set; }
    }
}
