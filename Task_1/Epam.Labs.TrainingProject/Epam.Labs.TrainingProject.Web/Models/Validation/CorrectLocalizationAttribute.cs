﻿using Epam.Labs.TrainingProject.Infrastructure.Interfaces;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;

namespace Epam.Labs.TrainingProject.Web.Models.Validation
{
    public class CorrectLocalizationAttribute : ValidationAttribute
    {
        // list with existing cultures
        private static List<string> existingCultures;

        public CorrectLocalizationAttribute()
        {
            //existingCultures = _cultureFacade.GetAllCulturesNames();
        }

        public override bool IsValid(object value)
        {
            if (value != null)
            {
                string strval = value.ToString();
                foreach (var name in existingCultures)
                {
                    if (strval == name)
                    {
                        return true;
                    }
                }
            }
            return false;
        }
    }
}
