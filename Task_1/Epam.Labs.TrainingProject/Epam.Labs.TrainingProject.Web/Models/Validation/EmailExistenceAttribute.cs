﻿using Epam.Labs.TrainingProject.Infrastructure.Interfaces;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;

namespace Epam.Labs.TrainingProject.Web.Models.Validation
{
    public class EmailExistenceAttribute : ValidationAttribute
    {
        // list with existing usernames
        private static List<string> existingEmails;
        
        public EmailExistenceAttribute()
        {
            //existingEmails = _accountFacade.GetAllExistingEmails();
        }

        public override bool IsValid(object value)
        {
            if (value != null)
            {
                string strval = value.ToString();
                foreach (var email in existingEmails)
                {
                    if (strval == email)
                    {
                        return false;
                    }
                }
            }
            return true;
        }
    }
}
