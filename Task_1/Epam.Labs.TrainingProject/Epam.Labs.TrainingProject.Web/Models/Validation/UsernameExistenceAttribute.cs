﻿using Epam.Labs.TrainingProject.Infrastructure.Interfaces;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System;
using Epam.Labs.TrainingProject.Web.Controllers;

namespace Epam.Labs.TrainingProject.Web.Models.Validation
{
    public class UsernameExistenceAttribute : ValidationAttribute
    {
        // list with existing usernames
        private List<string> _existingUsernames;

        public UsernameExistenceAttribute()
        {
            // _existingUsernames = ;
        }

        public UsernameExistenceAttribute(string ErrorMessage)
        {
            this.ErrorMessage = ErrorMessage;
        }

        public override bool IsValid(object value)
        {
            if (value != null)
            {
                string strval = value.ToString();
                foreach (var name in _existingUsernames)
                {
                    if (strval == name)
                    {
                        return false;
                    }
                }
            }
            return true;
        }
    }
}
