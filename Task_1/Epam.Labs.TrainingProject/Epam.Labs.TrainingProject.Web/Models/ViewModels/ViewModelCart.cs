﻿using Epam.Labs.TrainingProject.AppCore.Entities;
using System.Collections.Generic;

namespace Epam.Labs.TrainingProject.Web.Models.ViewModels
{
    public class ViewModelCart
    {
        public IEnumerable<Order> CartItems { get; set; }
        public int CartItemsAmount { get; set; }
        public decimal TotalCost { get; set; }
    }
}
