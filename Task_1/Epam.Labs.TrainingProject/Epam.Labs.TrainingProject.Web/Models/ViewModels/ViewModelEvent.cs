﻿using Epam.Labs.TrainingProject.AppCore.Entities;
using Epam.Labs.TrainingProject.Infrastructure.DTO;
using System.Collections.Generic;

namespace Epam.Labs.TrainingProject.Web.Models.ViewModels
{
    public class ViewModelEvent
    {
        public EventDTO CurrentEvent;
        public IEnumerable<Event> TopEventsOfCategory;
    }
}
