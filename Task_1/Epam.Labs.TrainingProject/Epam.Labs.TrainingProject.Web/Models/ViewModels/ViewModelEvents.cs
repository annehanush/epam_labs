﻿using Epam.Labs.TrainingProject.AppCore.Entities;
using Epam.Labs.TrainingProject.Web.Models.PaginationViewModels;
using System.Collections.Generic;

namespace Epam.Labs.TrainingProject.Web.Models.ViewModels
{
    public class ViewModelEvents
    {
        public IEnumerable<Event> Events { get; set; }
        public string CategoryName { get; set; }
        public PageViewModel PageViewModel { get; set; }
        public int RowsAmount { get; set; }
    }
}
