﻿using Epam.Labs.TrainingProject.AppCore.Entities;
using System.Collections.Generic;

namespace Epam.Labs.TrainingProject.Web.Models.ViewModels
{
    public class ViewModelIndex
    {
        public List<Event> UpcomingEvents { get; set; }
    }
}
